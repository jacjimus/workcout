<?php

namespace App;


class Utility 
{
    public static function renderAlert($alertType, $message){
        return "<div class='alert alert-$alertType fade in'>
							<button class='close' data-dismiss='alert'>×</button>
							<i class='icon ion-checkmark-circled'></i>
							$message
						</div>";
    }
}
