<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class Town extends Model
{
    protected $table = 'towns';
    protected $primaryKey = 'town_id';
    
        public static function get_All(){
        return DB::table('towns')->paginate(10);
    }
    
    public static function get_town($id){
        return DB::table('towns')
            ->selectRaw("*")
            ->where('town_id', $id)
            ;
    }
}
