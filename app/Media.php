<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Media extends Model
{
    public static function getImage($user_id){
        return DB::table('media')->where('user_id',$user_id)->pluck('name');
    }
}
