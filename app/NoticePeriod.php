<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoticePeriod extends Model
{
    protected $table = 'notice_period';
}
