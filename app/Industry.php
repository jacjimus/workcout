<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    protected $table = 'industries';
    
    protected $primaryKey = 'industry_id';
    
    public static function get_All(){
        return DB::table('industries')->paginate(10);
    }

}