<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Education extends Model
{
    protected $table = 'education';

    public static function educationInformationUserMeta($user_id){
        return DB::table('education')
            ->select(
                'education.id as id',
                'institution',
                'major',
                'completion_date',
                'education_certification.title as certification_attained',
                'countries.title as country',
                'grade',
                'towns.title as city',
                'description'
            )
            ->leftJoin('towns', 'towns.town_id', '=', 'education.town_id')
            ->leftJoin('education_certification', 'education_certification.id', '=', 'education.education_certification_id')
            ->leftJoin('countries', 'countries.country_id', '=', 'education.country_id')
            ->where('user_id', $user_id)
            ->get();
    }
}
