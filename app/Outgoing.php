<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Outgoing extends Model
{
    // outgoing db table
    protected $table = 'outgoing';

    //Outgoing primaryKey field
    protected $primaryKey = 'outgoing_id';


    /** Get all outbound messages
     * @param $query_filter
     * @return array;
     */
    public static function fetch_all_outgoing($query_filter = null){
        return DB::table('outgoing')
            ->orderby('outgoing_id', 'desc')
            ->paginate(10);
    }
}
