<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class TargetJob extends Model
{
    protected $table = 'applicants_target_job';

    public static function get_target_job_by_userID($user_id){
        return DB::table('applicants_target_job')
            ->select(
                'career_objectives',
                'job_title',
                'job_type.title as job_type_name',
                'towns.title as town_name',
                'industries.title as industry_name',
                'career_levels.title as career_level_name'
            )
            ->join('currency', 'currency.currency_id', '=', 'applicants_target_job.target_monthly_salary_currency_id')
            ->join('industries', 'industries.industry_id', '=', 'applicants_target_job.industry_id')
            ->join('career_levels', 'career_levels.career_level_id', '=', 'applicants_target_job.career_level_id')
            ->join('job_type', 'job_type.job_type_id', '=', 'applicants_target_job.job_type_id')
            ->join('towns', 'towns.town_id', '=', 'applicants_target_job.town_id')
            ->where('user_id', $user_id)
            ->first();
    }
}
