<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    // Get the table name
    protected $table = 'audit_logs';

    //Get the primary key of the table
    protected $primaryKey = 'audit_id';


    /** Get all the audit logs stored on the system
     * @param string $query_string
     * @return array
     */
    public static function fetch_audits($query_string = null){
        return DB::table('audit_logs')
            ->select('audit_id', 'action', 'description', 'users.name as user', 'audit_logs.created_at')
            ->leftJoin('users', 'audit_logs.user_id', '=', 'users.id')
            ->orderby('audit_id', 'desc')
            ->paginate(10);
    }

}
