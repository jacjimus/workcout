<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use DB;
use Illuminate\Support\Facades\Hash;

class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
    
    public static function get_All_users(){
        return DB::table('users')->paginate(10);
    }
    
    public static function get_Roles(){
        return DB::table('roles')->get();
    }


    public static function create_user($email, $password, $name,  $role_id){
        $new_user = new User();
        $new_user->email = $email;
        $new_user->password = Hash::make($password);
        $new_user->name = $name;
        $new_user->role = $role_id;

        //The status is assumed inactive until the user verifies via email
        $new_user->status = 0;
        if($new_user->save())
            return $new_user->id;
        return false;
    }

    public static function attempt_activation($email){
        if(self::is_already_activated($email)){
            return json_encode(['status' => 'error',
                'message' => 'Your account has already been activated. '
                    . 'Sign in to access your account']);
        } else {
            DB::table('users')->where('email',$email)
                ->update(
                    array('status' => 1)
                );

            return json_encode(['status' => 'success',
                'message' => 'You have successfully activated your account. '
                    . 'Please login and start the journey to find your dream career']);
        }
    }

    public static function is_already_activated($email){
        $status = DB::table('users')
            ->where('email', $email)
            ->pluck('status');

        return ($status == 1) ? true : false;
    }
    
    
}
