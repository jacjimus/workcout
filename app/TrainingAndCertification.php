<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingAndCertification extends Model
{
    protected $table = 'training_and_certifications';
}
