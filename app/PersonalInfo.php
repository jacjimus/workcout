<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class PersonalInfo extends Model
{
    protected  $table = 'personal_information';

    public static function get_personalInfomation_By_userID($user_id){
        return DB::table('personal_information')
            ->selectRaw("full_names,
                sex,
                countries.title as country_of_origin,
                towns.title as town_name,
                (select title from countries where personal_information.country_of_residence_id = countries.country_id limit 1) as country_of_residence,
                marital_status,
                no_of_dependents,
                date_of_birth")
            ->join('countries', 'countries.country_id', '=', 'personal_information.country_of_origin_id')
            ->join('towns', 'towns.town_id', '=', 'personal_information.town_id')
            ->where('user_id', $user_id)
            ->first();
    }


}
