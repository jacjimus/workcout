<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Job extends Model
{
    protected $table = 'jobs';
    
    public static function fetchJobs($queryFilter){
        $rawQueryString = $queryFilter;
        return DB::table('jobs')
        ->select(
            'job_id', 
            'job_title', 
            'currency.title AS currency', 
            'timeframe.title AS timeframe', 
            'salary_range_min', 
            'job_description',
            'jobs.created_at',
            'recruitment_type',
            'external_url',
            'mini_summary',
            'salary_range_max', 
            'additional_information', 
            'contract_type.title AS contract_type',
            'job_type.title AS job_type', 
            'industries.title AS industry', 
            'job_category.title as job_category', 
            'employer_summary', 
            'towns.title AS town', 
            'countries.title AS country', 
            'career_levels.title AS career_level', 
            'education_level.title as education_level')
        ->whereRaw($rawQueryString)
        ->leftJoin('contract_type', 'jobs.contract_type_id', '=', 'contract_type.contract_type_id')
        ->leftJoin('job_type', 'jobs.job_type_id', '=', 'job_type.job_type_id')
        ->leftJoin('industries', 'jobs.industry_id', '=', 'industries.industry_id')
        ->leftJoin('job_category', 'jobs.job_category_id', '=', 'job_category.job_category_id')
        ->leftJoin('towns', 'jobs.town_id', '=', 'towns.town_id')
        ->leftJoin('countries', 'jobs.country_id', '=', 'countries.country_id')
        ->leftJoin('career_levels', 'jobs.seniority_id', '=', 'career_levels.career_level_id')
        ->leftJoin('education_level', 'jobs.education_level_id', '=', 'education_level.education_level_id')
        ->leftJoin('currency', 'jobs.currency_id', '=', 'currency.currency_id')
        ->leftJoin('timeframe', 'jobs.timeframe_id', '=', 'timeframe.timeframe_id')
        ->orderBy('job_id', 'desc')
        ->paginate(10);
    }
    
    
    
    /** Get job title 
     * @param int $jobID
     * @return string name
     */
    public static function getName($jobID){
        return DB::table('jobs')
            ->where('job_id', $jobID)
            ->value('job_title');
    }
    
    
    
    /** Get all job applicants using jobID
     * 
     * @param int $jobID
     * @return array
     */
    public static function get_applications($jobID, $status){
        return DB::table('job_applications')
            ->select('job_applications.id as id','job_id',
                    'job_applications.user_id','full_names','email', 
                    'job_applications.created_at as application_date', 
                    'job_applications.status')
            ->leftJoin('personal_information', 'personal_information.user_id', '=', 'job_applications.user_id')
            ->leftJoin('users', 'job_applications.user_id', '=', 'users.id')
            ->where('job_id', $jobID)
            ->where('job_applications.status', $status)
            ->get();
    }



    public static function fetch_matching_resumes($jobID){

        //Get job parameters
        $job = DB::table('jobs')
            ->where('job_id', $jobID)
            ->first();

        return DB::table('users')
            ->select('users.email', 'users.id as user_id', 'personal_information.full_names')
            ->leftJoin('applicants_target_job', 'users.id', '=', 'applicants_target_job.user_id')
            ->leftJoin('personal_information', 'users.id', '=', 'personal_information.user_id')
            ->where('role', 1) //Fetch only job applicants
            ->where('applicants_target_job.industry_id', $job->industry_id)
            ->where('applicants_target_job.town_id', $job->town_id)
            ->where('applicants_target_job.job_category_id', $job->job_category_id)
            ->groupBy('users.id')
            ->get();
    }
    
    
    
    /** Get all job applicants using jobID
     * @param int $id
     * @return array
     */
    public static function get_Applicant_Info($id){
        return DB::table('job_applications')
            ->select('job_applications.id as id','job_id','job_applications.user_id','full_names','email', 'job_applications.created_at as application_date', 'job_applications.status')
            ->leftJoin('personal_information', 'personal_information.user_id', '=', 'job_applications.user_id')
            ->leftJoin('users', 'job_applications.user_id', '=', 'users.id')
            ->where('job_applications.id', $id)
            ->first();
    }
    
    
    
    public static function update_Applicant_status($id, $status){
        return DB::table('job_applications')
            ->where('id', $id)
            ->update(array('status' => $status));
            
    }
    
    
    
    public static function add_applicant_note($user_id, $note){
        DB::table('notes')
            ->insert(
                array('note'=>$note,'user_id'=>$user_id, 
                    'created_at' => date('Y-m-d H:i:s')));
    }
    
    
    
    public static function get_user_notes($user_id){
        return DB::table('notes')
            ->where('user_id', $user_id)
            ->get();
    }
    
    
    
    public static function getQuestionnaire($jobID){
        return DB::table('questionnaire')
            ->where('job_id', $jobID)
            ->get();
    }


    public static function get_job_status($status){
        if($status == 0)
            return 'Pending';
        else if($status == 1)
            return 'Shortlisted';
        else if($status == 2)
            return 'Interviewing';
        else if ($status == 3)
            return 'On-boarding';
        else
            return 'N/A';
    }
}
