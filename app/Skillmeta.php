<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Skillmeta extends Model
{
   

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'skills_meta';
     protected $primaryKey = 'id';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   
    
    
    public static function get_All(){
        return DB::table('skills_meta')->paginate(10);
    
    }
    
}
