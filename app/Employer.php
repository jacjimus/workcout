<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use DB;

class Employer extends Model
{
    
    protected $table = 'employers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
   
    
    public static function get_All(){
        return DB::table('employers')->paginate(10);
    }
    
    public static function getEmployer($user_id){
        return DB::table('employers')
            ->select("*")
            ->where('person_id', $user_id)
            ->get();
    }
}
