<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Answers extends Model
{
    protected $table = 'answers';
    
    public static function getFor($id){
        return DB::table('answers')
            ->where('question_id', $id)
            ->get();
    }
}
