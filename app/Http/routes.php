<?php

Route::any('/', ['as' => 'home','uses' => 'IndexController@index']);

Route::get('/dashboard', ['as' => 'dashboard','uses' => 'IndexController@dashboard']);

Route::get('/profile', ['as' => 'profile','uses' => 'IndexController@profile']);

Route::get('/jobs', ['as' => 'jobs','uses' => 'IndexController@jobs']);

Route::get('/logout', ['as' => 'logout','uses' => 'IndexController@logout']);

Route::get('/applications/{job_id}', array('as' => 'applications','uses' => 'IndexController@applications'));

Route::any('/user/{user_id?}', array('as'=>'user', 'uses' => 'IndexController@user'));

Route::get('/resume/{user_id}', array('as' => 'resume','uses' => 'IndexController@resume'));

Route::get('/resumes', array('as' => 'resumes','uses' => 'IndexController@resumes'));

Route::any('/authenticate_user', array('as' => 'authenticate_user','uses' => 'IndexController@verifyUser'));

Route::any('/add_job', ['as' => 'add_job','uses' => 'IndexController@addJob']);

Route::any('/questionnaire/{job_id}', ['as' => 'ques',  'uses' => 'IndexController@questionnaire']);

Route::any('/users', ['uses' => 'IndexController@users']);

Route::any('/process_application/{id?}', ['uses' => 'IndexController@process_application']);

// added for routing to categories
Route::any('/categories', ['as'=> 'categories', 'uses' => 'IndexController@categories']);
Route::any('/add_cat', ['as'=> 'add_cat', 'uses' => 'IndexController@add_category']);
Route::any('/update_cat/{job_category_id}', [ 'as' => 'update_jc', 'uses' => 'IndexController@update_cat']);
Route::get('/delete_cat/{job_category_id}', ['uses' => 'IndexController@delete_cat']);

// added for routing to towns
Route::any('/towns', ['as'=> 'towns', 'uses' => 'IndexController@towns']);
Route::any('/add_town', ['as'=> 'add_town', 'uses' => 'IndexController@add_town']);
Route::any('/update_town/{town_id}', [ 'as' => 'town_update', 'uses' => 'IndexController@update_town']);
Route::get('/delete_town/{town_id}', ['uses' => 'IndexController@delete_town']);


// added for routing to industries
Route::any('/industries', ['as'=> 'industries', 'uses' => 'IndexController@industries']);
Route::any('/add_industry', ['as'=> 'add_indus', 'uses' => 'IndexController@add_industry']);
Route::any('/update_indus/{industry_id}', [ 'as' => 'industry_update', 'uses' => 'IndexController@update_indus']);
Route::get('/delete_indus/{industry_id}', ['uses' => 'IndexController@delete_indus']);

// added for routing to skills
Route::any('/skills', ['as'=> 'skills', 'uses' => 'IndexController@skills']);
Route::any('/add_skill', ['as'=> 'add_skill', 'uses' => 'IndexController@add_skill']);
Route::any('/update_skill/{id}', [ 'as' => 'skill_update', 'uses' => 'IndexController@update_skill']);
Route::get('/delete_skill/{id}', ['uses' => 'IndexController@delete_skill']);

Route::get('/audit_logs', ['uses' => 'IndexController@audit_logs']);
Route::get('/outgoing', ['uses' => 'IndexController@outgoing']);
//employer_activation
Route::get('/employer_activation/{encrypted_email}', ['uses' => 'IndexController@employer_activation']);



// employer signup

Route::any('/signup', array('as' => 'signup', 'uses' => 'IndexController@signup'));

Route::any('/add_questionnaire/{id}', ['as' => 'add_questinaire','uses' => 'IndexController@add_questionnaire']);
Route::any('/edit_questionnaire/{id}', ['as' => 'edit_questinaire','uses' => 'IndexController@edit_questionnaire']);
Route::get('/del_que/{id}', ['uses' => 'IndexController@del_que']);

Route::any('/add_choice/{ans_id}', ['as' => 'add_choice','uses' => 'IndexController@add_choice']);
Route::any('/edit_ans/{id}', ['uses' => 'IndexController@edit_ans']);
Route::get('/del_ans/{id}', ['uses' => 'IndexController@del_ans']);

//
Route::any('/edit_cpy/{id}', ['as' => 'edit_cpy','uses' => 'IndexController@edit_company']);