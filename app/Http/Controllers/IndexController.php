<?php

/**
 * Developed by: Slevian Global Ltd
 * email: info@slevian.com
 * URL: www.slevian.com
 */
namespace App\Http\Controllers;
use App\Employer;
use App\Answers;
use App\Questioannaire;
use App\Audit;
use App\Categories;
use App\CareerLevel;
use App\ContactInformation;
use App\Country;
use App\Currency;
use App\Education;
use App\EducationCertification;
use App\Experience;
use App\Hobbies;
use App\Industry;
use App\Outgoing;
use App\Skillmeta;
use App\Media;
use App\JobType;
use App\Membership;
use App\NoticePeriod;
use App\PersonalInfo;
use App\Reference;
use App\Skill;
use App\TargetJob;
use App\Town;
use App\TrainingAndCertification;
use App\VideoLink;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use DB;
use Illuminate\Support\Facades\Hash;
use App\Utility;
use App\Job;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class IndexController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    private $userID;


    public function __construct(){
        $this->userID = null;
        $this->middleware('auth', ['except' => ['employer_activation','signup','index', 'verifyUser']]);
    }



    public function index() {
        //Display the main login page
        return view('index');
    }



    public function verifyUser(Request $request) {
        if ($request->isMethod('post')) {
            $email = $request->input('email');
            $password = $request->input('password');
            if (Auth::attempt(['email' => $email, 'password' => $password, 'status' => 1])) {
                if ($request->user() && $request->user()->role != 1) {
                    $request->session()->set('user_id', $request->user()->id);
                    $request->session()->set('username', $request->user()->name);
                    $request->session()->set('role', $request->user()->role);
                    if($request->user()->role != 4)
                    return redirect()->route('dashboard');
                    else {
                    $request->session()->set('emp',Employer::firstOrCreate(['person_id' => $request->user()->id])->id);
                    return redirect()->route('profile'); 
                    }
                }
            } else {
                $message = Utility::renderAlert('danger', '<strong>Login failed</strong> Wrong account or password');
                $request->session()->flash('_login', $message);
            }
        }
        return view('index');
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function dashboard() {
        //Load the main Dashboard
        return view('dashboard');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function profile(Request $request) {
        //Load the main Dashboard
        $user = $request->session()->get('user_id');
        $account = User::find($user);
        $employer = Employer::firstOrCreate(['person_id' => $user]);
       // var_dump($employer);die;
        return view('profile' , ['account' => $account])->with("employer" , $employer);
    }



    public function logout(Request $request) {
        Auth::logout();
        $request->session()->flush();
        return redirect()->route('home');
    }



    public function jobs(Request $request) {
        //Load the main Dashboard
        $filters = " 1=1 ";
        $emp_id = $request->session()->get('emp');
            if($request->session()->get('role') == 4)
                $filters = " employer_id = $emp_id ";
        return view('grids.jobs', [
            'title' => 'Jobs Posted',
            'jobsPosted' => \App\Job::fetchJobs($filters)]);
    }



    public function applications(Request $request, $jobId) {
        $jobName = Job::getName($jobId);
        $pending = Job::get_applications($jobId,0);
        $shortlisted= Job::get_applications($jobId,1);
        $interviewed = Job::get_applications($jobId,2);
        $onboarded = Job::get_applications($jobId,3);
        $resume_matches = Job::fetch_matching_resumes($jobId);

        //dd($resume_match);
        
        return view('grids.applications', [
            'title' => $jobName . ' applications',
            'pending' => $pending,
            'shortlisted' => $shortlisted,
            'interviewed' => $interviewed,
            'onboarded' => $onboarded,
            'resume_matches' => $resume_matches,
            'jobsPosted' => \App\Job::fetchJobs("")]);
    }
    
    
    public function users(){
        return View('grids.users', 
            array(
                'title' => 'Users', 
                'users' => User::get_All_users()));
    }
    


    public function user(Request $request, $user_id=null) {
        $user_meta = [];
        if(isset($user_id)){
                $user_meta = DB::table('users')
                    ->where('id', $user_id)
                    ->first();
            }
            
        $viewData = array(
            'title' => 'System user',
            'user_id' => $user_id,
            'user_meta' => $user_meta
        );
        
        
        if ($request->isMethod('post')) {

            $user_id = $request->input('user_id');
            
 
            if(isset($user_id) && $user_id > 0){               
                DB::table('users')
                    ->where('id', $user_id)
                    ->update(
                        array(
                            'email' => $request->input('email'), 
                            'name' => $request->input('full_names'),
                            'status' => $request->input('status'), 
                            'role' => $request->input('role')
                        )
                    );
            } else{
                
                $random_password = $request->input('password');
                $hashed_password = Hash::make($random_password);
            
                DB::table('users')->insert(
                    array(
                        'password' => $hashed_password,
                        'name' => $request->input('full_names'),
                        'email' => $request->input('email'),
                        'status' => $request->input('status'),
                        'role' => $request->input('role')
                ));
            }
            
            
            return redirect()->to('users/')
                ->with('message', 'User has been created successfully.');
        }
        
        
        return view('forms.user', $viewData);
    }


    
    public function resumes(){ 
        
         $resumes = DB::table('personal_information')
               ->select(DB::raw('full_names, email,sex, 
                       personal_information.user_id,
                       (select industries.title from industries where industries.industry_id = experience.industry_id limit 1) as industry_title,
                       (select job_category.title from job_category where job_category.job_category_id = applicants_target_job.job_category_id limit 1) as job_category_title'
                       ))
               ->join('users', 'personal_information.user_id','=','users.id')
               ->leftJoin('applicants_target_job', 'personal_information.user_id','=','applicants_target_job.user_id')
               ->leftJoin('experience', 'personal_information.user_id','=','experience.user_id')
               ->groupBy('personal_information.user_id')
               ->where('status', 1)
               ->where('role', 1)
               ->paginate('20');
         
       return View('grids.resumes', 
               array(
                   'title' => 'Job resumes', 
                   'resumes' => $resumes));
    }
    
    
    
    public function process_application(Request $request, $id = null){
        $applicants_details = Job::get_Applicant_Info($id);

        $status =  $request->input('status');
        if ($request->isMethod('post')) {   
            
            Job::update_Applicant_status($request->input('id'), $status);
            
            if($request->input('note') != ''){  //Do not save a blank note
                Job::add_applicant_note($request->input('user_id'), $request->input('note'));    
            }

            //log action

            $audit_action = new Audit();

            $audit_action->action = 'Process Application';
            $audit_action->description = "Candidate status has been set as : ".Job::get_job_status($status);
            $audit_action->user_id = $request->session()->get('user_id');
            $audit_action->save();

            return redirect()->to('applications/'.$request->input('job_id'))
                ->with('message', 'Application has been processed successfully');
        }
        
        return view('forms.application', 
                array(
                    'title' => ' Process Application', 
                    'applicants_details' => $applicants_details));
    }



    public function audit_logs(Request $request){
        return view('grids.audit_logs',
            array(
                'title' => ' Audit Log',
                'audit_logs' => Audit::fetch_audits()));
    }



    public function outgoing(Request $request){
        return view('grids.outgoing',
            array(
                'title' => ' Outgoing messages',
                'outgoing' => Outgoing::fetch_all_outgoing()));
    }



    public function questionnaire(Request $request, $jobId) {

        $questionnaire = Job::getQuestionnaire($jobId);
        $job = Job::getName($jobId);
        return view('questionnaire', array('title' => $job, 'questionnaire' => $questionnaire , 'id' => $jobId));
    }

    
    
    public function resume(Request $request, $user_id) {

        $this->userID = $user_id;
        $video_link = VideoLink::where('user_id', $this->userID)->first();
        $email = User::where('id', $this->userID)->pluck('email');
        $experiences = Experience::where('user_id', $this->userID)->get();
        $education_data = Education::educationInformationUserMeta($this->userID);
        $full_names = PersonalInfo::where('user_id', $this->userID)->pluck('full_names');
        $career_title = TargetJob::where('user_id', $this->userID)->pluck('job_title');
        $referenceInformationUserMeta = Reference::where('user_id', $this->userID)->get();
        $trainningUserMeta = TrainingAndCertification::where('user_id', $this->userID)->get();
        $membershipInformationUserMeta = Membership::where('user_id', $this->userID)->get();
        $skillsInformationMeta = Skill::where('user_id', $this->userID)->get();
        $hobbiesInformationMeta = Hobbies::where('user_id', $this->userID)->pluck('hobbies');

        return view('resume', [
            'notes' => Job::get_user_notes($user_id),
            'title' => (isset($full_names) && $full_names != '')
                ? $full_names . ' resume'
                : 'Incomplete resume',
            'website' => ContactInformation::where('user_id', $this->userID)->pluck('website'),
            'email' => (isset($email) && $email != '') ? $email : '',
            'full_names' => (isset($full_names) && $full_names != '')
                ? $full_names :
                'Full names not yet provided',
            'career_title' => (isset($career_title) && $career_title != '')
                ? $career_title
                : 'Please provide your desired career title',
            'target_job_data' => TargetJob::get_target_job_by_userID($this->userID),
            'experiences' => $experiences,
            'trainningUserMeta' => $trainningUserMeta,
            'referenceInformationUserMeta' => $referenceInformationUserMeta,
            'skillsInformationMeta' => $skillsInformationMeta,
            'hobbiesInformationMeta' => $hobbiesInformationMeta,
            'membershipInformationUserMeta' => $membershipInformationUserMeta,
            'educationInformationUserMeta' => $education_data,
            'personal_information_data' => PersonalInfo::get_personalInfomation_By_userID($this->userID),
            'contact_information_data' => ContactInformation::where('user_id', $this->userID)->first(),
            'video_link' => (isset($video_link) && $video_link != '')
                ? "<a href='$video_link->url'>Profile Link</a>"
                : "<a href='" . url('/video_link') . "'>Add profile video link</a>"
                ]
        );
    }



    public function addJob(Request $request) {
        if ($request->isMethod('post')) {
            
            $newJob = new Job();
            
            $newJob->job_title = $request->input('job_title');
            $newJob->job_description = $request->input('job_description');
            $newJob->town_id = $request->input('town_id');
            $newJob->country_id = $request->input('country_id');
            $newJob->company_name = $request->input('company_name');
            $newJob->mini_summary = $request->input('mini_summary');
            $newJob->salary_range_min = $request->input('salary_range_min');
            $newJob->salary_range_max = $request->input('salary_range_max');
            $newJob->currency_id = $request->input('currency_id');
            $newJob->industry_id = $request->input('industry_id');
            $newJob->job_category_id = $request->input('job_category_id');


            
            if($newJob->save()){


                $inserted_job_ID = $newJob->id;


                $tags = $request->input('skills', '');
                //convert to array
                $skills = explode(',', $tags);

                //Insert the skills in the job_skills map
                for($i=0; $i < count($skills); $i++){
                    DB::table('job_skill_map')
                        ->insert(
                            ['job_id' => $inserted_job_ID, 'skills' => $skills[$i]]);
                }


                $message = $message = Utility::renderAlert('success',
                    '<strong>Success</strong> Job post has been successfully created');
                return redirect()->route('jobs')->with('job_creation', $message);
            } else {
                $message = $message = Utility::renderAlert('danger',
                    '<strong>Error</strong> creating job. Please contact the administrator');
                return redirect()->route('jobs')->with('job_creation', $message);
            }
        }
        
        return view('forms.job', [
            'title' => 'Job',
            'towns' => Town::all(),
            'countries' => Country::all(),
            'industries' =>Industry::all(),
            'currencies' => Currency::all(),
            'categories' => DB::table('job_category')->get()
            ]);
    }



    public function add_category(Request $request) {
        
         $new = new Categories();
        if ($request->isMethod('post')) {
            
            $new->title = $request->input('title');
            
            
            if($new->save()){
                $message = $message = Utility::renderAlert(
                    'success',
                    '<strong>Success</strong> Job category has been successfully created');
                return redirect()->route('categories')->with('category_creation', $message);
            } else {
                $message = $message = Utility::renderAlert(
                    'danger',
                    '<strong>Error</strong> creating job. Please contact the administrator');
                return redirect()->route('categories')->with('category_creation', $message);
            }
        }
        
        return view('forms.category', [
            'title' => 'Create category',
           ])->with('category', $new);
    }


     public function update_cat(Request $request, $job_category_id) {
        
         $category = Categories::find($job_category_id);
         if ($request->isMethod('post')) {

            $this->validate($request, [
                'title' => 'required',
                        ]);

            $category->title = $request->title;
            
            if($category->save()){
                $message =  Utility::renderAlert(
                    'success', '<strong>Success</strong> Job category has beed updated');
                return redirect()
                    ->route('categories')
                    ->with('category_creation', $message);
            } else {
                $message = Utility::renderAlert(
                    'danger', '<strong>Error</strong> updating job category. Please contact the administrator');
                return redirect()->route('categories')
                    ->with('category_creation', $message);
            }
        }
        return view('forms.category', [
            'title' => 'Edit job category',
            
            ])
                ->with("category" , $category);
    }



    public function delete_cat($category_id){
        $del = DB::table('job_category')
            ->where('job_category_id',$category_id)->delete();
    
        if($del){
            $message =  Utility::renderAlert(
                'success', '<strong>Success</strong> Job category has beed deleted');
                
            return redirect()
                ->route('categories')
                ->with('category_creation', $message);
            
        }else {
            $message = Utility::renderAlert('danger',
                '<strong>Error</strong> deleting job category. Please contact the administrator');
                
            return redirect()->route('categories')
                ->with('category_creation', $message);
           
        }
    }



    public function add_town(Request $request) {
        
         $new = new Town();
        if ($request->isMethod('post')) {
            
            $new->title = $request->input('title');
            
            
            if($new->save()){
                $message = $message = Utility::renderAlert('success',
                    '<strong>Success</strong> Town/city has been successfully created');
                return redirect()->route('towns')->with('town_creation', $message);
            } else {
                $message = $message = Utility::renderAlert('danger',
                    '<strong>Error</strong> creating job. Please contact the administrator');
                return redirect()->route('towns')
                    ->with('town_creation', $message);
            }
        }
        
        return view('forms.town', [
            'title' => 'Create town',
           ])->with('town', $new);
    }



     public function update_town(Request $request, $town_id) {
        
         $town = Town::find($town_id);
         if ($request->isMethod('post')) {

            $this->validate($request, [
                'title' => 'required',
                        ]);

            $town->title = $request->title;
            
            if($town->save()){
                $message =  Utility::renderAlert('success',
                    '<strong>Success</strong> Town has beed updated');
                return redirect()->route('towns')->with('town_creation', $message);
            } else {
                $message = Utility::renderAlert('danger',
                    '<strong>Error</strong> updating town. Please contact the administrator');
                return redirect()->route('towns')
                    ->with('town_creation', $message);
            }
        }
        return view('forms.town', [
            'title' => 'Edit town',
            
            ])
                ->with("town" , $town);
    }



    public function delete_town($town_id){
        $del = DB::table('towns')->where('town_id',$town_id)->delete();
    
        if($del){
            $message =  Utility::renderAlert('success',
                '<strong>Success</strong> Town/city has beed deleted');
                
            return redirect()->route('towns')->with('town_creation', $message);
            
        }else {
            $message = Utility::renderAlert('danger',
                '<strong>Error</strong> deleting town/city. Please contact the administrator');
                
            return redirect()->route('towns')
                ->with('town_creation', $message);
           
        }
    }



    public function add_industry(Request $request) {
        
         $new = new Industry();
        if ($request->isMethod('post')) {
            
            $new->title = $request->input('title');
            
            
            if($new->save()){
                $message = $message = Utility::renderAlert('success',
                    '<strong>Success</strong> Job industry has been successfully created');
                return redirect()->route('industries')->with('industry_creation', $message);
            } else {
                $message = $message = Utility::renderAlert('danger',
                    '<strong>Error</strong> creating job industry. Please contact the administrator');
                return redirect()->route('industries')->with('industry_creation', $message);
            }
        }
        
        return view('forms.industry', [
            'title' => 'Create job industry',
           ])->with('industry', $new);
    }
        


     public function update_indus(Request $request, $industry_id) {
        
         $industry = Industry::find($industry_id);
         if ($request->isMethod('post')) {

            $this->validate($request, [
                'title' => 'required',
                        ]);

            $industry->title = $request->title;
            
            if($industry->save()){
                $message =  Utility::renderAlert('success',
                    '<strong>Success</strong> Industry has beed updated');
                return redirect()->route('industries')
                    ->with('industry_creation', $message);
            } else {
                $message = Utility::renderAlert('danger',
                    '<strong>Error</strong> updating industry. Please contact the administrator');
                return redirect()->route('industries')->with('industry_creation', $message);
            }
        }
        return view('forms.industry', [
            'title' => 'Edit Industry',
            
            ])
                ->with("industry" , $industry);
    }



    public function delete_indus($id){
        $del = DB::table('industries')->where('industry_id',$id)->delete();
    
        if($del){
            $message =  Utility::renderAlert('success',
                '<strong>Success</strong> Job industry has beed deleted');
                
            return redirect()->route('industries')->with('industry_creation', $message);
            
        }else {
            $message = Utility::renderAlert('danger',
                '<strong>Error</strong> deleting job industry. Please contact the administrator');
                
            return redirect()->route('industries')
                ->with('industry_creation', $message);
           
        }
    }



    public function add_skill(Request $request) {
        
         $new = new Skillmeta();
        if ($request->isMethod('post')) {
            
            $new->skill = $request->input('skill');
            
            
            if($new->save()){
                $message = $message = Utility::renderAlert('success',
                    '<strong>Success</strong> Job skill has been successfully created');
                return redirect()->route('skills')->with('skill_creation', $message);
            } else {
                $message = $message = Utility::renderAlert('danger',
                    '<strong>Error</strong> creating job skill. Please contact the administrator');
                return redirect()->route('skills')->with('skill_creation', $message);
            }
        }
        
        return view('forms.skill', [
            'title' => 'Create job skill',
           ])->with('skill', $new);
    }



    public function update_skill( $id, Request $request) {
        
         $skill = Skillmeta::find($id);
         if ($request->isMethod('post')) {
             //var_dump($skill);die;
            $this->validate($request, [
                'skill' => 'required',
                        ]);

            $skill->skill = $request->skill;
            if($skill->save()){
                $message =  Utility::renderAlert('success', '<strong>Success</strong> skill has beed updated');
                return redirect()->route('skills')->with('skill_creation', $message);
            } else {
                $message = Utility::renderAlert(
                    'danger', '<strong>Error</strong> updating skill. Please contact the administrator');
                return redirect()->route('skills')->with('skill_creation', $message);
            }
        }
        return view('forms.skill', [
            'title' => 'Edit skill',
            
            ])
                ->with("skill" , $skill);
    }



    public function delete_skill($id){
        $del = DB::table('skills_meta')->where('id',$id)->delete();
    
        if($del){
            $message =  Utility::renderAlert('success',
                '<strong>Success</strong> Job skill has beed deleted');
                
            return redirect()
                ->route('skills')
                ->with('skill_creation', $message);
            
        }else {
            $message = Utility::renderAlert('danger',
                '<strong>Error</strong> deleting job skill. Please contact the administrator');
                
            return redirect()->route('skills')->with('skill_creation', $message);
           
        }
    }
    


    public function categories(){
        return View('grids.categories',
            array('title' => 'Job Categories',
                'categories' => Categories::get_All_categories()));
    }



    public function towns(){
        return View('grids.towns',
            array('title' => 'Towns / Cities',
                'towns' => Town::get_All()));
    }


    public function industries(){
        return View('grids.industry',
            array('title' => 'Job industries',
                'industries' => Industry::get_All()));
    }



    public function skills(){
        return View('grids.skills',
            array('title' => 'Job Skills',
                'skills' => Skillmeta::get_All()));
    }

// New additions

    public function signup(Request $request) {

        if ($request->isMethod('post')) {

            $name = $request->input('firstname') . " " . $request->input('lastname') ;
            $email = $request->input('email');
            $password = $request->input('password');
            // $validator = Validator::make();
            $role = 4;
            if($acc = User::create_user($email, $password, $name,  $role)){

                $employer = new Employer;
                $employer->companyname = $request->input('companyname');
                $employer->companylocation = $request->input('companylocation');
                $employer->companysize = $request->input('companysize');
                $employer->companytype = $request->input('companytype');
                $employer->phonenumber = $request->input('phonenumber');
                $employer->jobtitle = $request->input('jobtitle');
                $employer->person_id = $acc;

            }


            if($employer->save()){
                $subject = 'Flexi Jobs employer registration';
                $link = url('employer_activation/'.Crypt::encrypt($email));
                $body = "Thank you for choosing Flexi Jobs as your
                    recruitment platform of choice. To activate your account please
                    click the link provided below <br />
                    <a href='$link'>Activate</a><br ><br /> or visit ".$link;
                $data = array('body' => $body);

                Mail::send('emails.registration', $data, function($message) use ($email, $subject) {
                    $message->from('flexi.jobs2016@gmail.com', 'Flexi Jobs');
                    $message->to($email, 'Flexi Jobs')->subject($subject);
                });

                //Log to outgoing
                $outgoing = new Outgoing();
                $outgoing->message = htmlentities($body);
                $outgoing->message_type = 'Email';
                $outgoing->recipient = $email;
                $outgoing->save();

                $message = Utility::renderAlert('success', 'Registration successful.
                Please visit your email to verify your account');

                return redirect()->route("home")->with('_login', $message);
            } else {
                $message = Utility::renderAlert('error', 'Registration failed! Try again later');
                $request->session()->flash('website_registration', $message);
            }
        }

        return view('forms.signup', [
            'title' => '',
        ]);
    }

    public function add_questionnaire($id , Request $request) {

        $new = new Questioannaire();

        if ($request->isMethod('post')) {

            $new->job_id = $request->input('job_id');
            $new->questionaire = $request->input('questionnaire');
            $new->status = 1;

            if($new->save()){
                $message = $message = Utility::renderAlert('success', '<strong>Success</strong> Job questioannaire has been successfully created');
                return redirect()->route('ques' ,['job_id' => $id] )->with('questioannaire_creation', $message);
            } else {
                $message = $message = Utility::renderAlert('danger', '<strong>Error</strong> creating job questioannaire. Please contact the administrator');
                return redirect()->route('ques' ,['job_id' => $id])->with('questioannaire_creation', $message);
            }
        }
        return view('forms.questionnaire', [
            'title' => 'Create job questionnaire', 'id' => $id
        ])->with('que', $new);
    }


    public function edit_questionnaire($id , Request $request) {

        $new = Questioannaire::find($id);

        if ($request->isMethod('post')) {

            $new->questionaire = $request->input('questionnaire');
            $new->status = 1;

            if($new->save()){
                $message = $message = Utility::renderAlert('success', '<strong>Success</strong> Job questionnaire has been successfully updated');
                return redirect()->route('ques' ,['job_id' => $new->job_id] )->with('questioannaire_creation', $message);
            } else {
                $message = $message = Utility::renderAlert('danger', '<strong>Error</strong> creating job questionnaire. Please contact the administrator');
                return redirect()->route('ques' ,['job_id' =>  $new->job_id])->with('questioannaire_creation', $message);
            }
        }

        return view('forms.questionnaire', [
            'title' => 'Edit job questionnaire', 'id' => $id
        ])->with('que', $new);
    }

    public function del_que($id){
        $job = Questioannaire::find($id)->job_id;
        $del = DB::table('questionnaire')->where('id',$id)->delete();

        if($del){
            $message =  Utility::renderAlert('success', '<strong>Success</strong> Job questionnaire has beed deleted');
            DB::table('answers')->where('question_id',$id)->delete();
            return redirect()->route('ques' , ['id' => $job])->with('questioannaire_creation', $message);

        }else {
            $message = Utility::renderAlert('danger', '<strong>Error</strong> deleting job questionnaire. Please contact the administrator');

            return redirect()->route('ques' , ['id' => $job])->with('questioannaire_creation', $message);

        }
    }

    public function add_choice($id, Request $request) {
        $new = new Answers ;

        if ($request->isMethod('post')) {

            $new->question_id = $request->input('question_id');
            $new->answer = $request->input('answer');
            if($new->save()){
                Utility::renderAlert('success', '<strong>Success</strong>Questioannaire choice has been saved');
                echo 'true';
            } else {
                echo 'false';
            }
        }
    }
    
    public function edit_company($id, Request $request) {
        $cpy = Employer::find($id) ;

        if ($request->isMethod('post')) {

            $cpy->about = $request->input('about');
            $cpy->vision = $request->input('vision');
            $cpy->mission = $request->input('mission');
            $cpy->corevalues = $request->input('corevalues');
            if($cpy->save()){
                $message = Utility::renderAlert('success', '<strong>Success</strong>Company details has been updated');
                $request->session()->flash('company_update', $message);
                echo 'true';
            } else {
                 $message = Utility::renderAlert('danger', '<strong>Error!</strong>Company details has not been updated');
                $request->session()->flash('company_update', $message);
                echo 'false';
            }
        }
    }

    public function del_ans($id){
        $ans = Answers::find($id);
        $job = Questioannaire::find($ans->question_id)->job_id;

        $del = DB::table('answers')->where('id', $id)->delete();

        if($del){
            $message =  Utility::renderAlert('success', '<strong>Success</strong> Questionnaire choice has beed deleted');
            return redirect()->route('ques' , ['id' => $job])->with('questioannaire_creation', $message);

        }else {
            $message = Utility::renderAlert('danger', '<strong>Error</strong> deleting questionnaire choice. Please contact the administrator');

            return redirect()->route('ques' , ['id' => $job])->with('questioannaire_creation', $message);

        }
    }

    public function employer_activation(Request $request, $encrypted_email){
        $decrypted_email = Crypt::decrypt($encrypted_email);
        $result = User::attempt_activation($decrypted_email);
        $response = json_decode($result);
        $message = Utility::renderAlert($response->status, $response->message);
        return redirect()->route('home')->with('_login', $message);
    }


}