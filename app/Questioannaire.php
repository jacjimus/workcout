<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Questioannaire extends Model
{
    protected $table = 'questionnaire';
    
    
    public static function getQuestionnaire($jobID){
        return DB::table('questionnaire')
            ->where('job_id', $jobID)
            ->get();
    }
}
