<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Categories extends Model
{
   

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'job_category';
    protected $primaryKey = 'job_category_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   
    
    
    public static function get_All_categories(){
        return DB::table('job_category')->paginate(10);
    }
    
    public static function get_job_category($id){
        return DB::table('job_category')
            ->selectRaw("*")
            ->where('job_category_id', $id)
            ;
    }
    
}
