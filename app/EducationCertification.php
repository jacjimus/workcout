<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducationCertification extends Model
{
    protected $table = 'education_certification';
}
