<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Skill extends Model
{
    protected $table = 'skills';

    public static function fetch_unique_skills(){
        $skills = DB::table('skills_meta')
            ->select('skill')
            ->distinct()
            ->get();

        $skillArray = [];
        $count = 0;
        foreach ($skills as $skill) {
            $count++;
            $skillArray[$count] = $skill->skill;
        }

        return array_flatten($skillArray);
    }

}
