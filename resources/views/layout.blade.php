<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie ie9" lang="en" class="no-js"> <![endif]-->
<!--[if !(IE)]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <head>
        <title>Flexi Jobs</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Flexi Jobs">
        <meta name="author" content="The Develovers">

        <!-- CSS -->
        <link href="{{url()}}/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="{{url()}}/assets/css/ionicons.css" rel="stylesheet" type="text/css">
        <link href="{{url()}}/assets/css/main.min.css" rel="stylesheet" type="text/css">

        <!-- Google Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,300,400,700' rel='stylesheet' type='text/css'>

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" type="{{url()}}/image/png" sizes="144x144" href="{{url()}}/assets/ico/queenadmin-favicon144x144.png">
        <link rel="apple-touch-icon-precomposed" type="{{url()}}/image/png" sizes="114x114" href="{{url()}}/assets/ico/queenadmin-favicon114x114.png">
        <link rel="apple-touch-icon-precomposed" type="{{url()}}/image/png" sizes="72x72" href="{{url()}}/assets/ico/queenadmin-favicon72x72.png">
        <link rel="apple-touch-icon-precomposed" type="{{url()}}/image/png" sizes="57x57" href="{{url()}}/assets/ico/queenadmin-favicon57x57.png">
        <link rel="shortcut icon" href="{{url()}}/assets/ico/favicon.ico">

        <style>
            body { font-family: Roboto; font-size: 12px; }
            .primary-content-heading h2 { font-family: Roboto; }
            .main-nav h3 { font-family: Roboto; }
        </style>
    </head>

    <body class="fixed-top-active dashboard">
        
        <?php
            $role = Session::get('role');
        ?>

        <!-- WRAPPER -->
        <div class="wrapper">
            <!-- TOP NAV BAR -->
            <nav class="top-bar navbar-fixed-top" role="navigation">
                <div class="logo-area">
                    <a href="#" id="btn-nav-sidebar-minified" class="btn btn-link btn-nav-sidebar-minified pull-left">
                        <i class="icon ion-arrow-swap"></i></a>
                    <a class="btn btn-link btn-off-canvas pull-left"><i class="icon ion-navicon"></i></a>
                    <div class="logo pull-left">
                        <a href="index-2.html">
                            <img src="{{ url('/assets/img/queenadmin-logo.png')}}" alt="Flexi Jobs" />
                        </a>
                    </div>
                </div>
                <form class="form-inline searchbox hidden-xs" role="form">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icon ion-ios7-search"></i></span>
                            <input type="search" class="form-control" placeholder="search the site ...">
                        </div>
                    </div>
                </form>
                <div class="top-bar-right pull-right">
                    <div class="action-group hidden-xs hidden-sm">
                        <ul>
                            <!-- skins -->
                            <li class="action-item skins">
                                <form id="style-switcher">
                                    <div class="btn-group">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon ion-ios7-gear"></i>
                                        </a>
                                        <div class="arrow"></div>
                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <p>
                                                    <strong>Dark Side Nav</strong>
                                                </p>
                                                <button type="button" class="btn-skin nav-dark full-dark" data-skin="{{url()}}/assets/css/skins/full-dark.css"></button>
                                                <button type="button" class="btn-skin nav-dark green" data-skin="{{url()}}/assets/css/skins/green.css"></button>
                                                <button type="button" class="btn-skin nav-dark blue" data-skin="{{url()}}/assets/css/skins/blue.css"></button>
                                                <button type="button" class="btn-skin nav-dark darkorange" data-skin="{{url()}}/assets/css/skins/darkorange.css"></button>
                                                <button type="button" class="btn-skin nav-dark seagreen" data-skin="{{url()}}/assets/css/skins/seagreen.css"></button>
                                            </li>
                                            <li>
                                                <p>
                                                    <strong>Light Side Nav</strong>
                                                </p>
                                                <button type="button" class="btn-skin nav-light full-white" data-skin="{{url()}}/assets/css/skins/full-white.css"></button>
                                                <button type="button" class="btn-skin nav-light green green-light-nav" data-skin="{{url()}}/assets/css/skins/green-light-nav.css"></button>
                                                <button type="button" class="btn-skin nav-light blue blue-light-nav" data-skin="{{url()}}/assets/css/skins/blue-light-nav.css"></button>
                                                <button type="button" class="btn-skin nav-light darkorange darkorange-light-nav" data-skin="{{url()}}/assets/css/skins/darkorange-light-nav.css"></button>
                                                <button type="button" class="btn-skin nav-light seagreen seagreen-light-nav" data-skin="{{url()}}/assets/css/skins/seagreen-light-nav.css"></button>
                                            </li>
                                            <li>
                                                <label class="fancy-checkbox">
                                                    <input type="checkbox" id="fixed-top-nav" checked="checked">
                                                    <span>Fixed Top Navigation</span>
                                                </label>
                                            </li>
                                            <li class="menu-item-footer"><a href="#" title="Reset Style" class="reset-style">Reset Style</a></li>
                                        </ul>
                                    </div>
                                </form>
                            </li>
                            <!-- end skins -->

                            
                        </ul>
                    </div>
                    <div class="logged-user">
                        <div class="btn-group">
                            <a href="#" class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                <span class="name"> {{ Session::get('username') }} <i class="icon ion-ios7-arrow-down"></i></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="#">
                                        <i class="icon ion-android-social-user"></i>
                                        <span class="text">Profile</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="icon ion-android-settings"></i>
                                        <span class="text">Change Password</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('/logout')}}">
                                        <i class="icon ion-power"></i>
                                        <span class="text">Logout</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="action-group visible-lg-inline-block">
                        <ul>
                            
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- END TOP NAV BAR -->

            <!-- COLUMN LEFT -->
            <div id="col-left" class="col-left">
                <nav id="main-nav" class="main-nav">

                    <h3>MAIN</h3>
                    <ul class="main-menu">
                        @if($role <> 4)
                        <li class="active">
                            <a href="{{ url('/dashboard') }}"><i class="icon ion-ios7-speedometer"></i><span class="text">Dashboards</span></a>
                        </li>
                        @else
                        <li class="active">
                            <a href="{{ url('/profile') }}"><i class="icon ion-ios7-settings"></i><span class="text">Profile</span></a>
                        </li>
                        @endif;
                        <li class="has-submenu">
                            <a href="#" class="submenu-toggle"><i class="icon ion-android-note"></i><span class="text">Jobs</span></a>
                            <ul class="list-unstyled sub-menu collapse">
                                <li>
                                    <a href="{{ url('/jobs') }}">
                                        <span class="text">Job Posts</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="form-inplace-editing.html">
                                        <span class="text">In-place Editing</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        
                        @if($role == 3)
                        <li class="has-submenu">
                            <a href="#" class="submenu-toggle"><i class="icon ion-person"></i><span class="text">System Users</span></a>
                            <ul class="list-unstyled sub-menu collapse">
                                <li>
                                    <a href="{{ url('/users') }}">
                                        <span class="text">Users</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('/audit_logs') }}">
                                        <span class="text">Audit logs</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        @endif
                        <li class="has-submenu">
                            <a href="#" class="submenu-toggle"><i class="icon ion-person"></i><span class="text">Resume Database</span></a>
                            <ul class="list-unstyled sub-menu collapse">
                                <li>
                                    <a href="{{url('/resumes')}}">
                                        <span class="text">All resumes</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>

                    @if($role != 4)

                    <h3>ESSENTIALS</h3>
                    <ul class="main-menu">
                        <li class="has-submenu">
                            <a href="#" class="submenu-toggle"><i class="icon ion-ios7-pie"></i><span class="text">Reports</span></a>
                            <ul class="list-unstyled sub-menu collapse">
                                <li class="active">
                                    <a href="{{url('/outgoing')}}">
                                        <span class="text">Outgoing Messages</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        @if($role != 4)
                        <li class="has-submenu">
                            <a href="#" class="submenu-toggle"><i class="icon ion-android-storage"></i><span class="text">Master Records</span></a>
                            <ul class="list-unstyled sub-menu collapse">
                                <li class="active">
                                   <a href="{{url('/categories')}}">
                                        <span class="text">Job Categories</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('/towns')}}">
                                        <span class="text">Towns/Cities</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('/industries')}}">
                                        <span class="text">Industries</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('/skills')}}">
                                        <span class="text">JOb Skills</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        @endif
                        
                        @if($role == 3)
                        <li><a href="#">
                                <i class="icon ion-settings"></i><span class="text">Configs</span></a></li>
                        @endif
                    </ul>

                        @endif

                </nav>
            </div>
            <!-- END COLUMN LEFT -->

            <!-- COLUMN RIGHT -->
            <div id="col-right" class="col-right ">
                @yield('content')
            </div>
            <!-- END COLUMN RIGHT -->
        </div>
        <!-- END WRAPPER -->

        <!-- Javascript -->
        <script src="{{url()}}/assets/js/jquery/jquery-2.1.0.min.js"></script>
        <script src="{{url()}}/assets/js/bootstrap/bootstrap.js"></script>
        <script src="{{url()}}/assets/js/plugins/bootstrap-multiselect/bootstrap-multiselect.js"></script>
        <script src="{{url()}}/assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="{{url()}}/assets/js/queen-common.min.js"></script>
        <script src="{{url()}}/assets/js/plugins/summernote/summernote.min.js"></script>
        <script src="{{url()}}/assets/js/plugins/markdown/markdown.js"></script>
	<script src="{{url()}}/assets/js/plugins/markdown/to-markdown.js"></script>
	<script src="{{url()}}/assets/js/plugins/markdown/bootstrap-markdown.js"></script>
        <script src="{{url()}}/assets/js/plugins/stat/flot/jquery.flot.min.js"></script>
        <script src="{{url()}}/assets/js/plugins/stat/flot/jquery.flot.resize.min.js"></script>
        <script src="{{url()}}/assets/js/plugins/stat/flot/jquery.flot.time.min.js"></script>
        <script src="{{url()}}/assets/js/plugins/stat/flot/jquery.flot.orderBars.js"></script>
        <script src="{{url()}}/assets/js/plugins/stat/flot/jquery.flot.tooltip.min.js"></script>
        <script src="{{url()}}/assets/js/plugins/mapael/raphael/raphael-min.js"></script>
        <script src="{{url()}}/assets/js/plugins/mapael/jquery.mapael.js"></script>
        <script src="{{url()}}/assets/js/plugins/mapael/maps/world_countries.js"></script>
        <script src="{{url()}}/assets/js/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
        <script src="{{url()}}/assets/js/plugins/moment/moment.min.js"></script>
        <script src="{{url()}}/assets/js/plugins/bootstrap-editable/bootstrap-editable.min.js"></script>
        <script src="{{url()}}/assets/js/plugins/jquery-maskedinput/jquery.masked-input.min.js"></script>
        <script src="{{url()}}/assets/js/queen-charts.min.js"></script>
        <script src="{{url()}}/assets/js/queen-maps.min.js"></script>
        <script src="{{url()}}/assets/js/queen-elements.min.js"></script>

        <script type="text/javascript">
            // code for confirming all deletes on master records
            function ConfirmDelete()
            {
                var x = confirm("Are you sure you want to delete?");
                if (x)
                    return true;
                else
                    return false;
            }
            // code for populating Questionnaire choices modal form's question_id hidden field
            $('#quick-add-questioannaire').on('show.bs.modal', function(e) {
                var question = $(e.relatedTarget).data('id');
                $("#question_id").val(question);
            });

            // Ajax code for submitting questionnaire choices modal data

            jQuery(function($) {
                $('#submit-data').click( function(event) {
                    var $form = $('#choice-form');
                    var $target = $($form.attr('data-target'));

                    $.ajax({
                        type: 'POST',
                        url: $form.attr('action') + "/" + $('input[id=ans_id]').val(),
                        data: {'answer':$('input[id=answer]').val(), 'question_id':$('input[id=question_id]').val(), '_token': $('input[name=_token]').val()},

                        success: function(data) {
                            if(data === 'true'){
                                $("#quick-add-questioannaire").modal('hide');
                                location.reload();
                            }
                            else
                                $("#quick-add-questioannaire").modal('show');
                        }
                    });

                    event.preventDefault();
                });
            });
            
            
             // Ajax code for submitting company details modal data

            jQuery(function($) {
                $('#save-data').click( function(event) {
                    var $form = $('#edit-form');
                    var about = $('textarea[name=about]').val();
                    var vision = $('input[name=vision]').val();
                    var mission = $('input[name=mission]').val();
                    var corevalues = $('input[name=corevalues]').val();
                    var _token = $('input[name=_token]').val();
                    var values = 'about='+about+'&vision='+vision+'&mission='+mission+'&corevalues='+corevalues+'&_token='+_token+'&';
                    $.ajax({
                        type: 'POST',
                        url: $form.attr('action') + "/" + $('input[name=company]').val(),
                        data: values,

                        success: function(data) {
                            if(data === 'true'){
                                $("#quick-edit").modal('hide');
                                location.reload();
                            }
                            else
                                $("#quick-edit").modal('show');
                        }
                    });

                    event.preventDefault();
                });
            });
        </script>
    </body>
</html>