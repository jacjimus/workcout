<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie ie9" lang="en" class="no-js"> <![endif]-->
<!--[if !(IE)]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->


    <!-- Mirrored from demo.thedevelovers.com/dashboard/queenadmin-1.1/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 12 Jun 2015 14:34:32 GMT -->
    <head>
        <title>Log In | QueenAdmin - Beautiful Admin Dashboard</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="QueenAdmin - Beautiful Bootstrap Admin Dashboard Theme">
        <meta name="author" content="The Develovers">

        <!-- CSS -->
        <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="assets/css/ionicons.css" rel="stylesheet" type="text/css">
        <link href="assets/css/main.min.css" rel="stylesheet" type="text/css">

        <!-- Google Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,300,400,700' rel='stylesheet' type='text/css'>

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/queenadmin-favicon144x144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/queenadmin-favicon114x114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/queenadmin-favicon72x72.png">
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="assets/ico/queenadmin-favicon57x57.png">
        <link rel="shortcut icon" href="assets/ico/favicon.html">

    </head>

    <body class="middle-content page-login">
        <div class="top-bar text-center">
            <a href="index-2.html">
                <img src="assets/img/queenadmin-logo.png" alt="QueenAdmin Logo">
            </a>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-5 col-sm-offset-1 col-lg-4 col-lg-offset-2">
                    <div class="content-box-bordered login-box box-with-help">
                        <h1>Log in to your account</h1>
                        <form class="form-horizontal" role="form" method="post" action="{{ url('/authenticate_user') }}">
                            <div class="form-group">
                                <label for="inputEmail3b" class="control-label sr-only">Email</label>
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon ion-email"></i></span>
                                        <input type="email" name="email" class="form-control" id="inputEmail3b" placeholder="Email">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3b" class="control-label sr-only">Password</label>
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon ion-locked"></i></span>
                                        <input type="password" name="password" class="form-control" id="inputPassword3b" placeholder="Password">
                                        <?php echo csrf_field(); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-7">
                                    <button type="submit" class="btn btn-success btn-block">Sign in</button>
                                </div>
                                <div class="col-xs-5 text-right">
                                    <a href="#">
                                        <em>forgot password?</em>
                                    </a>
                                </div>
                            </div>
                            <p>
                                <em>Don't have an account yet?</em>
                                <a href="{{url('/signup')}}">
                                    <strong>Employer Sign Up</strong>
                                </a>
                            </p>
                        <button type="submit" class="btn btn-link btn-login-help"><i class="icon ion-help-circled"></i></button>
                        </form>
                    </div>
                    
                    {!! Session::get('_login') !!}
                </div>
                <div class="col-sm-5 col-lg-4">
                    <div class="login-copytext">
                        <h2>Flexi-Job Portal <small>Where careers are made</small></h2>
                        <p>Manage applications, system users, audit security logs and employer module</p>
                        <h2>Another Heading Text</h2>
                    </div>
                </div>
            </div>
        </div>

        <!-- Javascript -->
        <script src="assets/js/jquery/jquery-2.1.0.min.js"></script>
        <script src="assets/js/bootstrap/bootstrap.js"></script>
        <script src="assets/js/queen-form-layouts.min.js"></script>

    </body>

    <!-- Mirrored from demo.thedevelovers.com/dashboard/queenadmin-1.1/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 12 Jun 2015 14:34:32 GMT -->
</html>