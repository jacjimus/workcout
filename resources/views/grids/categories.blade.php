@extends('layout')
@section('content')
<div class="container-fluid primary-content">
    <div class="primary-content-heading clearfix">
        <h2>{{ $title }}</h2>
        @if (session('category_creation'))
            {!! session('category_creation') !!}
        @endif
        <ul class="breadcrumb pull-left">
            <li><i class="icon ion-home"></i><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Dashboard v1</li>
        </ul>
        <div class="sticky-content pull-right">
            <div class="btn-group btn-dropdown">
                <button type="button" class="btn btn-default btn-sm btn-favorites" data-toggle="dropdown"><i class="icon ion-android-star"></i> Summary</button>
                <ul class="dropdown-menu dropdown-menu-right list-inline">
                    <li><a href="#"><i class="icon ion-pie-graph"></i> <span>Statistics</span></a></li>
                    <li><a href="#"><i class="icon ion-android-inbox"></i> <span>Inbox</span></a></li>
                    <li><a href="#"><i class="icon ion-chatboxes"></i> <span>Chat</span></a></li>
                    <li><a href="#"><i class="icon ion-help-circled"></i> <span>Help</span></a></li>
                    <li><a href="#"><i class="icon ion-gear-a"></i> <span>Settings</span></a></li>
                    <li><a href="#"><i class="icon ion-help-buoy"></i> <span>Support</span></a></li>
                </ul>
            </div>
            <a href="{{url('/add_cat/')}}">
            <button type="button" class="btn btn-default btn-sm btn-quick-task" >
                <i class="icon ion-plus-round"></i> Add New</button>
            </a>
        </div>
        <!-- quick task modal -->
        <div class="modal fade" id="quick-task-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Quick Task</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="task-title" class="control-label sr-only">Title</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="task-title" placeholder="Title">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label sr-only">Description</label>
                                <div class="col-sm-12">
                                    <textarea class="form-control" name="task-description" rows="5" cols="30" placeholder="Description"></textarea>
                                </div>
                            </div>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save Task</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- end quick task modal -->
    </div>
    <!-- END PRIMARY CONTENT HEADING -->

    <div class="widget widget-no-header widget-transparent bottom-30px">
        <!-- QUICK SUMMARY INFO -->

        <!-- END QUICK SUMMARY INFO -->
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="widget">
                <div class="widget-header clearfix">
                    <h3><i class="icon ion-android-storage"></i> 
                        <span>Jobs Categories</span>
                    </h3>
                    <div class="btn-group widget-header-toolbar visible-lg">
                        <a href="#" title="Expand/Collapse" class="btn btn-link btn-toggle-expand"><i class="icon ion-ios7-arrow-up"></i></a>
                        <a href="#" title="Remove" class="btn btn-link btn-remove"><i class="icon ion-ios7-close-empty"></i></a>
                    </div>
                </div>
                <div class="widget-content">

                    
                                <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Job Category title</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($categories as $cat)
                                    <tr>
                                        <td>{{ $cat->title }}</td>
                                        
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary btn-md">Action</button>
                                                <button type="button" class="btn btn-primary btn-md dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="{{ url('/update_cat/'.$cat->job_category_id) }}">update</a></li>
                                                    <li><a href="{{ url('/delete_cat/'.$cat->job_category_id) }}" onclick= 'return ConfirmDelete()'>Delete</a></li>
                                                    
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                                    
                                 {!! $categories->setPath('categories') !!}   
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
