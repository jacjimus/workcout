@extends('layout')
@section('content')
<div class="container-fluid primary-content">
    <!-- PRIMARY CONTENT HEADING -->
    <div class="primary-content-heading clearfix">
        <h2>{{ $title }}</h2>
        
        @if (session('job_creation'))
            {!! session('job_creation') !!}
        @endif
        <ul class="breadcrumb pull-left">
            <li><i class="icon ion-home"></i><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Dashboard v1</li>
        </ul>
        <div class="sticky-content pull-right">
           
            <a href="{{url('/add_job')}}" class="btn btn-default btn-sm btn-quick-task" ><i class="icon ion-android-add"></i> Add New</a>
        </div>
        <!-- quick task modal -->
        
        <!-- end quick task modal -->
    </div>
    <!-- END PRIMARY CONTENT HEADING -->

    <div class="widget widget-no-header widget-transparent bottom-30px">
        <!-- QUICK SUMMARY INFO -->

        <!-- END QUICK SUMMARY INFO -->
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header clearfix">
                    <h3><i class="icon ion-android-storage"></i> 
                        <span>Jobs Posted</span>
                    </h3>
                    <div class="btn-group widget-header-toolbar visible-lg">
                        <a href="#" title="Expand/Collapse" class="btn btn-link btn-toggle-expand"><i class="icon ion-ios7-arrow-up"></i></a>
                        <a href="#" title="Remove" class="btn btn-link btn-remove"><i class="icon ion-ios7-close-empty"></i></a>
                    </div>
                </div>
                <div class="widget-content">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Date Posted</th>
                                    <th>Title</th>
                                    <th>Industry</th>
                                    <th>Job Category</th>
                                    <th>Job Location</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($jobsPosted as $jobMeta)
                                <tr>
                                    <td>{{ $jobMeta->created_at }}</td>
                                    <td>{{ $jobMeta->job_title }}</td>
                                    <td>{{ $jobMeta->industry }}</td>
                                    <td>{{ $jobMeta->job_category }}</td>
                                    <td>{{ $jobMeta->town }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary btn-sm">Action</button>
                                            <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="{{ url('/applications/'.$jobMeta->job_id) }}">Applications</a></li>
                                                <li><a href="#">View Full info</a></li>
                                                <li><a href="{{url('/questionnaire/'.$jobMeta->job_id)}}">questionnaire</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Edit</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </div></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <?php
                        $jobsPosted->setPath(url('/jobs'));
                        
                        ?>
                        {!! $jobsPosted->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection