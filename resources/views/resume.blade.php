@extends('layout')
@section('content')
<div class="container-fluid primary-content">
    <div class="primary-content-heading clearfix">
        <h2>{{ $title }}</h2>
        <ul class="breadcrumb pull-left">
            <li><i class="icon ion-home"></i><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Dashboard v1</li>
        </ul>
        <div class="sticky-content pull-right">
            <div class="btn-group btn-dropdown">
                <button type="button" class="btn btn-default btn-sm btn-favorites" data-toggle="dropdown"><i class="icon ion-android-star"></i> Summary</button>
                <ul class="dropdown-menu dropdown-menu-right list-inline">
                    <li><a href="#"><i class="icon ion-pie-graph"></i> <span>Statistics</span></a></li>
                    <li><a href="#"><i class="icon ion-android-inbox"></i> <span>Inbox</span></a></li>
                    <li><a href="#"><i class="icon ion-chatboxes"></i> <span>Chat</span></a></li>
                    <li><a href="#"><i class="icon ion-help-circled"></i> <span>Help</span></a></li>
                    <li><a href="#"><i class="icon ion-gear-a"></i> <span>Settings</span></a></li>
                    <li><a href="#"><i class="icon ion-help-buoy"></i> <span>Support</span></a></li>
                </ul>
            </div>
            <button type="button" class="btn btn-default btn-sm btn-quick-task" data-toggle="modal" data-target="#quick-task-modal">
                <i class="icon ion-edit"></i> Add New</button>
        </div>
        <!-- quick task modal -->
        <div class="modal fade" id="quick-task-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Quick Task</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="task-title" class="control-label sr-only">Title</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="task-title" placeholder="Title">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label sr-only">Description</label>
                                <div class="col-sm-12">
                                    <textarea class="form-control" name="task-description" rows="5" cols="30" placeholder="Description"></textarea>
                                </div>
                            </div>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save Task</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- end quick task modal -->
    </div>
    <!-- END PRIMARY CONTENT HEADING -->

    <div class="widget widget-no-header widget-transparent bottom-30px">
        <!-- QUICK SUMMARY INFO -->

        <!-- END QUICK SUMMARY INFO -->
    </div>

    <div class="row">
        <div class="col-md-8">
            <div class="widget">
                <div class="widget-header clearfix">
                    <h3><i class="icon ion-android-storage"></i> 
                        <span>Applicants full profile</span>
                    </h3>
                    <div class="btn-group widget-header-toolbar visible-lg">
                        <a href="#" title="Expand/Collapse" class="btn btn-link btn-toggle-expand"><i class="icon ion-ios7-arrow-up"></i></a>
                        <a href="#" title="Remove" class="btn btn-link btn-remove"><i class="icon ion-ios7-close-empty"></i></a>
                    </div>
                </div>
                <div class="widget-content">

                    <div class="container" id="printable">

                        <div class="eight columns" >

                            <div class="padding-right">

                                <h3>Personal Information</h3>
                                <p class="margin-reset">
                                    <strong>Names: {{ (isset($personal_information_data->full_names)) ? $personal_information_data->full_names : '' }}</strong>
                                </p>

                                <p class="margin-reset">
                                    <strong>Date Of Birth</strong> 
                                    {{date("F jS, Y",strtotime((isset($personal_information_data->date_of_birth)) ? $personal_information_data->date_of_birth : ''))}}
                                </p>

                                <p class="margin-reset">
                                    <strong>Country of origin</strong> 
                                    {{ (isset($personal_information_data->country_of_origin)) ? $personal_information_data->country_of_origin : '' }}
                                </p>

                                <p class="margin-reset">
                                    <strong>Country of residence</strong> 
                                    {{ (isset($personal_information_data->country_of_residence)) ? $personal_information_data->country_of_residence : '' }}
                                </p>

                                <p class="margin-reset">
                                    <strong>City</strong> 
                                    {{ (isset($personal_information_data->town_name)) ? $personal_information_data->town_name : '' }}
                                </p>

                                <p class="margin-reset">
                                    <strong>Marital Status</strong> 
                                    {{ (isset($personal_information_data->marital_status)) ? ucfirst(strtolower($personal_information_data->marital_status)) : '' }}
                                </p>

                                <p class="margin-reset">
                                    <strong>Dependents</strong> 
                                    {{ (isset($personal_information_data->no_of_dependents)) ? $personal_information_data->no_of_dependents : '' }}
                                </p>
                                <hr />

                            </div>


                            <div class="padding-right">

                                <h3 class="margin-bottom-15">Contact Information</h3>
                                <p class="margin-reset">
                                    <strong>Email Address: </strong>{{ $email  }}
                                </p>

                                <p class="margin-reset">
                                    <strong>Mobile</strong> {{ (isset($contact_information_data->mobile)) ? $contact_information_data->mobile : '' }}
                                </p>

                                <p class="margin-reset">
                                    <strong>Phone</strong> {{ (isset($contact_information_data->phone)) ? $contact_information_data->phone : '' }}
                                </p>

                                <p class="margin-reset">
                                    <strong>Physical Address</strong> {{ (isset($contact_information_data->physical_address)) ? $contact_information_data->physical_address : '' }}
                                </p>

                                <p class="margin-reset">
                                    <strong>Postal Address</strong> {{ (isset($contact_information_data->postal_address)) ? $contact_information_data->postal_address : '' }}
                                </p>

                                <p class="margin-reset">
                                    <strong>Postal code</strong> {{ (isset($contact_information_data->postal_code)) ? $contact_information_data->postal_code : '' }}
                                </p>

                                <p class="margin-reset">
                                    <strong>Website</strong> {{ (isset($contact_information_data->website)) ? $contact_information_data->website : '' }}
                                </p>
                                <hr />

                            </div>

                            <div class="padding-right">

                                <h3 class="margin-bottom-15">Target Job</h3>
                                <p class="margin-reset">
                                <h5>Title: {{ (isset($target_job_data->job_title)) ? $target_job_data->job_title : '' }}</h5>
                                </p>

                                <p class="margin-reset">
                                    <strong>Career Level</strong> {{ (isset($target_job_data->career_level_name)) ? $target_job_data->career_level_name : '' }}
                                </p>

                                <p class="margin-reset">
                                    <strong>Target job location</strong> {{ (isset($target_job_data->town_name)) ? $target_job_data->town_name : '' }}
                                </p>

                                <p class="margin-reset">
                                    <strong>Career objectives</strong> {{ (isset($target_job_data->career_objectives)) ? $target_job_data->career_objectives : '' }}
                                </p>

                                <p class="margin-reset">
                                    <strong>Target Industry</strong> {{ (isset($target_job_data->industry_name)) ? $target_job_data->industry_name : '' }}
                                </p>

                                <p class="margin-reset">
                                    <strong>Employment / Job Type</strong> {{ (isset($target_job_data->job_type_name)) ? $target_job_data->job_type_name : '' }}
                                </p>
                                <hr />

                            </div>

                            <div class="padding-right">
                                <h3 class="margin-bottom-15">Work experience</h3>
                                @foreach($experiences as $experience)
                                <h5>Company name: {{ $experience->company_name }} <small>
                                        ({{date("F jS, Y",strtotime($experience->start_date))}} - {{date("F jS, Y",strtotime($experience->end_date))}})</small></h5>
                                </p><br/>
                                <p class="margin-reset">
                                    <strong>Position: </strong> {{ $experience->position }}
                                </p>
                                <p class="margin-reset">
                                    <strong>Description: </strong> {{ $experience->description }}
                                </p>
                                @endforeach
                                <hr />
                            </div>


                            <div class="padding-right">
                                <h3 class="margin-bottom-15">Skills</h3>
                                <p class="margin-reset">
                                    <strong>Skills: </strong>
                                <div class="skills">
                                    @foreach($skillsInformationMeta as $skill)
                                    <span style='margin: 3px;'>{{ $skill->skills }}</span>
                                    @endforeach
                                </div>
                                </p><br /><br />
                                <hr />
                            </div>


                            <div class="padding-right">
                                <h3 class="margin-bottom-15">Hobbies</h3>
                                <p class="margin-reset">

                                <div class="skills">

                                    <?php
                                    $hobbies = explode(',', $hobbiesInformationMeta);

                                    for ($i = 0; $i < count($hobbies); $i++) {
                                        ?><span><?php echo $hobbies[$i]; ?></span> <?php
                                    }
                                    ?>
                                </div>
                                </p><br /><br />
                                <hr />
                            </div>


                            <div class="padding-right">
                                <h3 class="margin-bottom-15">References</h3>
                                @foreach($referenceInformationUserMeta as $referenceMetaItem)
                                <p class="margin-reset">
                                    <strong>Name: </strong> {{ $referenceMetaItem->name }}
                                </p>
                                <p class="margin-reset">
                                    <strong>Job Title: </strong> {{ $referenceMetaItem->job_title }}
                                </p>
                                <p class="margin-reset">
                                    <strong>Company name: </strong> {{ $referenceMetaItem->company_name }}
                                </p>
                                <p class="margin-reset">
                                    <strong>Phone number: </strong> {{ $referenceMetaItem->phone_number }}
                                </p>
                                @endforeach
                                <hr />
                            </div>

                            <div class="padding-right">
                                <h3 class="margin-bottom-15">Membership</h3>
                                @foreach($membershipInformationUserMeta as $membershipMetaItem)
                                <p class="margin-reset">
                                    <strong>Organization: </strong> {{ $membershipMetaItem->organization }}
                                </p>
                                <p class="margin-reset">
                                    <strong>Role: </strong> {{ $membershipMetaItem->role }}
                                </p>
                                <p class="margin-reset">
                                    <strong>Member since: </strong>{{date("F jS, Y",strtotime($membershipMetaItem->member_since))}}
                                </p>
                                @endforeach
                                <hr />
                            </div>


                            <div class="padding-right">
                                <h3 class="margin-bottom-15">Video Profile</h3>
                                <p class="margin-reset">
                                <h5>Link: {!! $video_link !!}</h5>
                                </p>
                                <hr />

                            </div>
                        </div>


                        <div class="eight columns">

                            <h3 class="margin-bottom-20">Education</h3>

                            <dl class="resume-table">
                                @foreach($educationInformationUserMeta as $educationMetaItem)
                                <dt>
                                <small class="date">Completion date {{date("F jS, Y",strtotime($educationMetaItem->completion_date))}}</small>
                                <strong>{{ $educationMetaItem->major }}</strong>
                                </dt>
                                <dd>
                                    <p>{{ $educationMetaItem->description }}<br />
                                        Academic certification: {{ $educationMetaItem->certification_attained }}<br />
                                        Grade: {{ $educationMetaItem->grade }}<br />
                                        {{ $educationMetaItem->institution }}, {{ $educationMetaItem->city }} {{ $educationMetaItem->country }} <br />
                                        @endforeach
                            </dl>


                            <h3 class="margin-bottom-20">Trainning and Certification</h3>

                            <!-- Resume Table -->
                            <dl class="resume-table">
                                @foreach($trainningUserMeta as $trainningMetaItem)
                                <dt>
                                <small class="date">{{date("F jS, Y",strtotime($trainningMetaItem->start_date))}} - {{date("F jS, Y",strtotime($trainningMetaItem->end_date))}}</small>
                                <strong>{{ $trainningMetaItem->title }}</strong>
                                </dt>
                                <dd>
                                    <p><strong>Institution: </strong>{{ $trainningMetaItem->name_of_institution }}</p>
                                    <p><strong>Name of Certificate: </strong> {{ $trainningMetaItem->name_of_certificate }}</p>
                                    <p><strong>Member since: </strong>{{date("F jS, Y",strtotime($trainningMetaItem->valid_until))}}</p>
                                </dd>
                                @endforeach
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="widget widget-live-feed">
                <div class="widget-header clearfix">
                    <h3><i class="icon ion-paper-airplane"></i> 
                        <span>Notes</span>
                    </h3>
                    <div class="btn-group widget-header-toolbar">
                        <a href="#" title="Refresh" class="btn btn-link"><i class="icon ion-ios7-refresh-empty"></i></a>
                        <a href="#" title="Expand/Collapse" class="btn btn-link btn-toggle-expand"><i class="icon ion-ios7-arrow-up"></i></a>
                        <a href="#" title="Remove" class="btn btn-link btn-remove"><i class="icon ion-ios7-close-empty"></i></a>
                    </div>
                </div>
                <div style="position: relative; overflow: hidden; width: auto;" class="slimScrollDiv">
                    <div style="overflow: hidden; width: auto;" class="widget-content">
                        @foreach($notes as $note)
                        <div class="media activity-item">
         
                            <i class="icon ion-chatboxes pull-left text-success"></i>
                            <div class="media-body">
                                <p class="activity-title">{{ $note->note }}</p>
                                <small class="text-muted">{{ $note->created_at }}</small>
                            </div>
                        </div>
                        @endforeach
                    </div> 
                </div>
            </div> 
        </div>
    </div>
</div>
@endsection