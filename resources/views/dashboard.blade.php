@extends('layout')
@section('content')
<div class="container-fluid primary-content">
    <!-- PRIMARY CONTENT HEADING -->
    <div class="primary-content-heading clearfix">
        <h2>DASHBOARD</h2>
        <ul class="breadcrumb pull-left">
            <li><i class="icon ion-home"></i><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Dashboard v1</li>
        </ul>
        <div class="sticky-content pull-right">
            <div class="btn-group btn-dropdown">
                <button type="button" class="btn btn-default btn-sm btn-favorites" data-toggle="dropdown"><i class="icon ion-android-star"></i> Favorites</button>
                <ul class="dropdown-menu dropdown-menu-right list-inline">
                    <li><a href="#"><i class="icon ion-pie-graph"></i> <span>Statistics</span></a></li>
                    <li><a href="#"><i class="icon ion-android-inbox"></i> <span>Inbox</span></a></li>
                    <li><a href="#"><i class="icon ion-chatboxes"></i> <span>Chat</span></a></li>
                    <li><a href="#"><i class="icon ion-help-circled"></i> <span>Help</span></a></li>
                    <li><a href="#"><i class="icon ion-gear-a"></i> <span>Settings</span></a></li>
                    <li><a href="#"><i class="icon ion-help-buoy"></i> <span>Support</span></a></li>
                </ul>
            </div>
            <button type="button" class="btn btn-default btn-sm btn-quick-task" data-toggle="modal" data-target="#quick-task-modal"><i class="icon ion-edit"></i> Quick Task</button>
        </div>
        <!-- quick task modal -->
        <div class="modal fade" id="quick-task-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Quick Task</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="task-title" class="control-label sr-only">Title</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="task-title" placeholder="Title">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label sr-only">Description</label>
                                <div class="col-sm-12">
                                    <textarea class="form-control" name="task-description" rows="5" cols="30" placeholder="Description"></textarea>
                                </div>
                            </div>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save Task</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- end quick task modal -->
    </div>
    <!-- END PRIMARY CONTENT HEADING -->

    <div class="widget widget-no-header widget-transparent bottom-30px">
        <!-- QUICK SUMMARY INFO -->
        <div class="widget-content">
            <h3 class="sr-only">QUICK SUMMARY INFO</h3>
            <div class="row">
                <div class="col-sm-3 text-center">
                    <div class="quick-info horizontal">
                        <i class="icon ion-thumbsup pull-left bg-seagreen"></i>
                        <p>3700 <span>New Profiles</span></p>
                    </div>
                </div>
                <div class="col-sm-3 text-center">
                    <div class="quick-info horizontal">
                        <i class="icon ion-arrow-graph-up-right pull-left bg-orange"></i>
                        <p>27% <span>New Applications</span></p>
                    </div>
                </div>
                <div class="col-sm-3 text-center">
                    <div class="quick-info horizontal">
                        <i class="icon ion-cash pull-left bg-green"></i>
                        <p>KES 5,400 <span>subscription</span></p>
                    </div>
                </div>
                <div class="col-sm-3 text-center">
                    <div class="quick-info horizontal">
                        <i class="icon ion-search pull-left bg-blue"></i>
                        <p>7,285 <span>Searches</span></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- END QUICK SUMMARY INFO -->
    </div>

    <div class="row">
        <div class="col-md-8">
            <!-- CHART WITH JUSTIFIED TAB -->
            <div class="widget">
                <div class="widget-header clearfix no-padding">
                    <h3 class="sr-only">
                        <span>SALES AND VISITS STAT</span>
                    </h3>
                    <ul id="dashboard-stat-tab" class="nav nav-pills nav-justified">
                        <li class=""><a href="#tab-sales" data-cid="#dashboard-sales-chart">Sales</a></li>
                        <li class="active"><a href="#tab-visits" data-cid="#dashboard-visits-chart">Visits</a></li>
                    </ul>
                </div>
                <div id="dashboard-stat-tab-content" class="widget-content tab-content">
                    <div class="tab-pane fade in active" id="tab-sales">
                        <div class="flot-chart" id="dashboard-sales-chart"></div>
                    </div>
                    <div class="tab-pane fade" id="tab-visits">
                        <div class="flot-chart" id="dashboard-visits-chart"></div>
                    </div>
                </div>
            </div>
            <!-- END CHART WITH JUSTIFIED TAB -->
        </div>
        <div class="col-md-4">
            <!-- ORDER STATUS -->
            <div class="widget">
                <div class="widget-header clearfix">
                    <h3><i class="icon ion-bag"></i> 
                        <span>Subscription Status</span>
                    </h3>
                    <div class="btn-group widget-header-toolbar">
                        <a href="#" title="Expand/Collapse" class="btn btn-link btn-toggle-expand"><i class="icon ion-ios7-arrow-up"></i></a>
                        <a href="#" title="Remove" class="btn btn-link btn-remove"><i class="icon ion-ios7-close-empty"></i></a>
                    </div>
                </div>
                <div class="widget-content">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>Status</th>
                                <th>Order Number</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><span class="label label-warning">Unprocessed</span></td>
                                <td><a href="#">ORD834580</a></td>
                                <td>KES 320</td>
                            </tr>
                            <tr>
                                <td><span class="label label-success">Completed</span></td>
                                <td><a href="#">ORD834565</a></td>
                                <td>KES 400</td>
                            </tr>
                            <tr>
                                <td><span class="label label-warning">Pending</span></td>
                                <td><a href="#">ORD834577</a></td>
                                <td>KES 80</td>
                            </tr>
                            <tr>
                                <td><span class="label label-danger">Error</span></td>
                                <td><a href="#">ORD834543</a></td>
                                <td>KES 307</td>
                            </tr>s
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END ORDER STATUS -->
        </div>
    </div>
</div>
<div class="right-sidebar">
    <!-- CHAT -->
    <div class="widget widget-chat-contacts">
        <div class="widget-header clearfix">
            <h3 class="sr-only">CHAT</h3>
            <div class="btn-group btn-group-justified widget-header-toolbar visible-lg">
                <div class="btn-group">
                    <button type="button" class="btn btn-primary btn-xs"><i class="icon ion-plus-circled"></i> Add</button>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn dropdown-toggle btn-xs btn-success" data-btnclass="btn-success" data-toggle="dropdown">Online
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right chat-status" role="menu">
                        <li><a href="#" class="online" data-btnclass="btn-success">Online</a></li>
                        <li><a href="#" class="away" data-btnclass="btn-warning">Away</a></li>
                        <li><a href="#" class="busy" data-btnclass="btn-danger">Busy</a></li>
                        <li><a href="#" class="offline" data-btnclass="btn-default">Offline</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="widget-content">
            <strong>Online (4)</strong>
            <ul class="list-unstyled chat-contacts">
                <li>
                    <a href="#" id="theusername"><img src="assets/img/user1.png" class="img-circle" alt="Antonius">Antonius</a>
                </li>
                <li>
                    <a href="#"><img src="assets/img/user2.png" class="img-circle" alt="Antonius">Michael Smith</a>
                </li>
                <li class="away">
                    <a href="#"><img src="assets/img/user3.png" class="img-circle" alt="Antonius">Stella Ray</a>
                </li>
                <li class="busy">
                    <a href="#"><img src="assets/img/user4.png" class="img-circle" alt="Antonius">Jane Doe</a>
                </li>
            </ul>
            <strong>Offline (6)</strong>
            <ul class="list-unstyled chat-contacts contacts-offline">
                <li>
                    <a href="#"><img src="assets/img/user5.png" class="img-circle" alt="John Simmons">John Simmons</a>
                </li>
                <li>
                    <a href="#"><img src="assets/img/user6.png" class="img-circle" alt="Jack Bay">Jack Bay</a>
                </li>
                <li>
                    <a href="#"><img src="assets/img/user7.png" class="img-circle" alt="Daraiana">Daraiana</a>
                </li>
                <li>
                    <a href="#"><img src="assets/img/user8.png" class="img-circle" alt="Alessio Ferrara">Alessio Ferrara</a>
                </li>
                <li>
                    <a href="#"><img src="assets/img/user9.png" class="img-circle" alt="Sorana">Sorana</a>
                </li>
                <li>
                    <a href="#"><img src="assets/img/user10.png" class="img-circle" alt="Regan Morton">Regan Morton</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- END CHAT -->
</div>
@endsection