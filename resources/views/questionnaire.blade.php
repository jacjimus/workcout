@extends('layout')
@section('content')
<div class="container-fluid primary-content">
    <div class="primary-content-heading clearfix">
        <h2>{{ $title }}</h2>
        
        @if (session('questioannaire_creation'))
            {!! session('questioannaire_creation') !!}
        @endif
        <ul class="breadcrumb pull-left">
            <li><i class="icon ion-home"></i><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Dashboard v1</li>
        </ul>
        <div class="sticky-content pull-right">
            <div class="btn-group btn-dropdown">
                <button type="button" class="btn btn-default btn-sm btn-favorites" data-toggle="dropdown"><i class="icon ion-android-star"></i> Summary</button>
                <ul class="dropdown-menu dropdown-menu-right list-inline">
                    <li><a href="#"><i class="icon ion-pie-graph"></i> <span>Statistics</span></a></li>
                    <li><a href="#"><i class="icon ion-android-inbox"></i> <span>Inbox</span></a></li>
                    <li><a href="#"><i class="icon ion-chatboxes"></i> <span>Chat</span></a></li>
                    <li><a href="#"><i class="icon ion-help-circled"></i> <span>Help</span></a></li>
                    <li><a href="#"><i class="icon ion-gear-a"></i> <span>Settings</span></a></li>
                    <li><a href="#"><i class="icon ion-help-buoy"></i> <span>Support</span></a></li>
                </ul>
            </div>
            <a href="{{url('/add_questionnaire/'. $id)}}" class="btn btn-default btn-sm btn-quick-task" ><i class="icon ion-android-add"></i> Add New</a>
        </div>
        <!-- quick task modal -->
        
        <!-- end quick task modal -->
    </div>
    <!-- END PRIMARY CONTENT HEADING -->

    <div class="widget widget-no-header widget-transparent bottom-30px">
        <!-- QUICK SUMMARY INFO -->

        <!-- END QUICK SUMMARY INFO -->
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header clearfix">
                    <h3><i class="icon ion-android-storage"></i> 
                        <span>{{$title}} Questionnaire</span>
                    </h3>
                    <div class="btn-group widget-header-toolbar visible-lg">
                        <a href="#" title="Expand/Collapse" class="btn btn-link btn-toggle-expand"><i class="icon ion-ios7-arrow-up"></i></a>
                        <a href="#" title="Remove" class="btn btn-link btn-remove"><i class="icon ion-ios7-close-empty"></i></a>
                    </div>
                </div>
                <!-- quick questioannaire modal -->
        <div class="modal fade" id="quick-add-questioannaire" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Add choice to the questioannaire</h4>
                    </div>
                    <div class="modal-body">
                        <form id="choice-form"  class="form-horizontal" role="form" action="{{url('/add_choice')}}">
                            <div class="form-group">
                                <label for="answer" class="control-label sr-only">Option</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="answer" required>
                                    <input type="hidden" id="question_id">
                                    <input type="hidden" id="type">
                                </div>
                            </div>
                            <?php echo csrf_field(); ?>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            &nbsp; &nbsp; &nbsp; &nbsp;
                            <button type="button" id="submit-data" class="btn btn-primary">Save choice</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- end quick add questioannaire -->
        
                <div class="widget-content">
                    <div class="table-responsive" id="questionaires">
                         
                        <ol>
                        @foreach($questionnaire as $item)
                        <li>
                            {{$item->questionaire}}&nbsp;&nbsp;&nbsp;&nbsp;<a hrerf='' style="cursor: pointer" title=" Add choices" data-toggle="modal" data-id="{{$item->id}}" data-name='add' data-target="#quick-add-questioannaire"><i class="icon ion-plus-circled"></i></a> 
                            &nbsp;&nbsp;&nbsp;&nbsp;<a href='{{url('/edit_questionnaire/' . $item->id)}}' style="cursor: pointer" title="Edit questioannaire"><i class="icon ion-edit orange"></i></a> 
                            &nbsp;&nbsp;&nbsp;&nbsp;<a href='{{url('/del_que/' . $item->id)}}' style="cursor: pointer" title="Delete questioannaire" onclick= 'return ConfirmDelete()'><i class="icon ion-trash-a orange"></i></a> 
                            <ol style="list-style: disc">
                                <table class="table-responsive">    
                            
                                <?php $answers = App\Answers::getFor($item->id);
                                    foreach($answers as $answer){
                                        ?>
                                <tr><td><li>{{ $answer->answer }} </li></td><td>&nbsp;&nbsp;<a href='{{url('del_ans/' . $answer->id)}}' style="cursor: pointer" title="Delete Choice" onclick= 'return ConfirmDelete()'><i class="icon ion-trash-a orange"></i></a></td></tr>
                                        <?php
                                    }
                                ?>
                                
                            </table>
                            </ol>
                        </li>
                        @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection