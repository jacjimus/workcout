@extends('layout')
@section('content')
<div class="container-fluid primary-content">
    <div class="primary-content-heading clearfix">
        <h2></h2>
        
        @if (session('company_update'))
            {!! session('company_update') !!}
        @endif
        <ul class="breadcrumb pull-left">
            <li><i class="icon ion-home"></i><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Dashboard v1</li>
        </ul>
        <div class="sticky-content pull-right">
            <div class="btn-group btn-dropdown">
                <button type="button" class="btn btn-default btn-sm btn-favorites" data-toggle="dropdown"><i class="icon ion-android-star"></i> Summary</button>
                <ul class="dropdown-menu dropdown-menu-right list-inline">
                    <li><a href="#"><i class="icon ion-pie-graph"></i> <span>Statistics</span></a></li>
                    <li><a href="#"><i class="icon ion-android-inbox"></i> <span>Inbox</span></a></li>
                    <li><a href="#"><i class="icon ion-chatboxes"></i> <span>Chat</span></a></li>
                    <li><a href="#"><i class="icon ion-help-circled"></i> <span>Help</span></a></li>
                    <li><a href="#"><i class="icon ion-gear-a"></i> <span>Settings</span></a></li>
                    <li><a href="#"><i class="icon ion-help-buoy"></i> <span>Support</span></a></li>
                </ul>
            </div>
             </div>
        <!-- quick task modal -->
        
        <!-- end quick task modal -->
    </div>
    <!-- END PRIMARY CONTENT HEADING -->

    <div class="widget widget-no-header widget-transparent bottom-30px">
        <!-- QUICK SUMMARY INFO -->

        <!-- END QUICK SUMMARY INFO -->
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="widget-content">
                    <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="icon ion-android-settings">&nbsp;</i>{{$employer->companyname}} Account</h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png" class="img-circle img-responsive"> </div>
                
                <!--<div class="col-xs-10 col-sm-10 hidden-md hidden-lg"> <br>
                  <dl>
                    <dt>DEPARTMENT:</dt>
                    <dd>Administrator</dd>
                    <dt>HIRE DATE</dt>
                    <dd>11/12/2013</dd>
                    <dt>DATE OF BIRTH</dt>
                       <dd>11/12/2013</dd>
                    <dt>GENDER</dt>
                    <dd>Male</dd>
                  </dl>
                </div>-->
                <div class=" col-md-9 col-lg-9 "> 
                  <table class="table table-user-information">
                    <tbody>
                        <tr><td colspan="2"><h3>Company Details:</h3></td></tr>
                      <tr>
                        <td>Company name:</td>
                        <td>{{$employer->companyname}}</td>
                      </tr>
                      <tr>
                        <td>Location:</td>
                        <td>{{$employer->companylocation}}</td>
                      </tr>
                      <tr>
                        <td>Size</td>
                        <td>{{$employer->companysize}}</td>
                      </tr>
                   
                         <tr>
                             <tr>
                        <td>Type</td>
                        <td>{{$employer->companytype}}</td>
                      </tr>
                      <tr>
                        <td>Location:</td>
                        <td>{{$employer->companylocation}}</td>
                      </tr>
                      <tr><td colspan="2" style="padding-left: 50% !important;"><a href='' style="cursor: pointer" title="Edit company details" data-toggle="modal" data-target="#quick-edit"><i class="icon ion-edit"></i>Edit company details</a></td></tr>
                      <tr>
                        <td>About us:</td>
                        <td>{{$employer->about}}</td>
                      </tr>
                      <tr>
                        <td>vision:</td>
                        <td>{{$employer->vision}}</td>
                      </tr>
                      <tr>
                        <td>Mission:</td>
                        <td>{{$employer->mission}}</td>
                      </tr>
                      <tr>
                        <td>Core Values:</td>
                        <td>{{$employer->corevalues}}</td>
                      </tr>
                      <tr><td colspan="2"><h3>Account Details:</h3></td></tr>
                        <tr>
                        <td>Name</td>
                        <td>{{$account->name}}</td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><a href="mailto:{{$account->email}}">{{$account->email}}</a></td>
                      </tr>
                      <tr>
                        <td>Job Title</td>
                        <td>{{$employer->jobtitle}}
                        </td>
                           
                      </tr>
                     
                    </tbody>
                  </table>
                  
                  <a href="#" class="btn btn-primary">My Sales Performance</a>
                  <a href="#" class="btn btn-primary">Team Sales Performance</a>
                </div>
              </div>
            </div>
                 <div class="panel-footer">
                        <a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                        <span class="pull-right">
                            <a href="edit.html" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                            <a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                        </span>
                    </div>
            
          </div>
                </div>
                </div>
                </div>
            
             <!-- quick questioannaire modal -->
        <div class="modal fade" id="quick-edit" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Edit {{$employer->companyname}} details</h4>
                    </div>
                    <div class="modal-body">
                        <form id="edit-form"  class="form-horizontal" role="form" action="{{url('/edit_cpy')}}">
                            <div class="form-group">
                                <label for="answer" class="control-label sr-only">About</label>
                                
                                <div class="col-sm-12">
                                    <textarea class="form-control" name='about' placeholder="About us" >{{$employer->about}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="answer" class="control-label sr-only">Vision</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="vision" placeholder="Vision" value="{{$employer->vision}}">
                                   
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="answer" class="control-label sr-only">Mission</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="mission" placeholder="Mission" value="{{$employer->mission}}">
                                   
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="answer" class="control-label sr-only">Core Values</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" name="corevalues" placeholder="Core values" value="{{$employer->corevalues}}" >
                                    <input type="hidden" name="company" value='{{$employer->id}}'>
                                    
                                </div>
                            </div>
                            <?php echo csrf_field(); ?>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            &nbsp; &nbsp; &nbsp; &nbsp;
                            <button type="button" id="save-data" class="btn btn-primary">Save details</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- end quick add questioannaire -->
            

@endsection