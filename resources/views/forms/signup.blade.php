<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie ie9" lang="en" class="no-js"> <![endif]-->
<!--[if !(IE)]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->


    <!-- Mirrored from demo.thedevelovers.com/dashboard/queenadmin-1.1/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 12 Jun 2015 14:34:32 GMT -->
    <head>
        <title>Signup</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="QueenAdmin - Beautiful Bootstrap Admin Dashboard Theme">
        <meta name="author" content="The Develovers">

        <!-- CSS -->
        <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="assets/css/ionicons.css" rel="stylesheet" type="text/css">
        <link href="assets/css/main.min.css" rel="stylesheet" type="text/css">

        <!-- Google Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,300,400,700' rel='stylesheet' type='text/css'>

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/queenadmin-favicon144x144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/queenadmin-favicon114x114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/queenadmin-favicon72x72.png">
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="assets/ico/queenadmin-favicon57x57.png">
        <link rel="shortcut icon" href="assets/ico/favicon.html">

    </head>

    <body class="middle-content">
        <div class="top-bar text-center">
            <a href="index-2.html">
                <img src="assets/img/queenadmin-logo.png" alt="QueenAdmin Logo">
            </a>
        </div>
        <div class="container-fluid">
            
            <div class="row">
                
                <div class="col-sm-6 col-sm-offset-1 col-lg-6 col-lg-offset-3">
                    
                    @if(Session::get('website_registration'))
                    {!! Session::get('website_registration')!!}
                    @else
                    <div class="content-box-bordered login-box box-with-help"  style="margin-top: 1em !important;">
                        <h1>Employers' registration</h1>
                        <form class="form-horizontal" role="form" method="post" action="">
                            <div class="form-group" style="float: right;">
                                <p>
                            <em>already registered?</em>
                            <a href="{{url('/')}}">
                                <strong>Sign In</strong>
                            </a>
                        </p>
                            </div>
                            <div class="form-group">
                                <label for="companyname" class="control-label sr-only">Company name</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon ion-home"></i></span>
                                        <input type="text" name="companyname" class="form-control" id="companyname" placeholder="Company Name">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="companylocation" class="control-label sr-only">Company location</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon ion-location"></i></span>
                                        <input type="text" name="companylocation" class="form-control" id="companylocation" placeholder="Company Location">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="companyname" class="control-label sr-only">Company size</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon ion-ios7-keypad"></i></span>
                                        <select  name="companysize" class="form-control" id="companysize" >
                                            <option value="">Company Size</option>
                                            <option value="0-10">0 - 10 employees</option>
                                            <option value="11-20">11 - 20 employees</option>
                                            <option value="21-50">21 - 50 employees</option>
                                            <option value="51-100">51 - 100 employees</option>
                                            <option value="101-200">101 - 200 employees</option>
                                            <option value="over 200">Over 200 employees</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="companytype" class="control-label sr-only">Company type</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon ion-android-share"></i></span>
                                        <select name="companytype" class="form-control" id="companytype">
                                            <option value="">Company type</option>
                                            <option value="public">Public</option>
                                            <option value="private">Private</option>
                                            <option value="ngo">NGO</option>
                                           </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="companyname" class="control-label sr-only"></label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon ion-ios7-telephone"></i></span>
                                        <input type="text" name="phonenumber" class="form-control" id="phonenumber" placeholder="Phone number">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="firstname" class="control-label sr-only">First name</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon ion-person"></i></span>
                                        <input type="text" name="firstname" class="form-control" id="firstname" placeholder="First name">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="lastname" class="control-label sr-only">Last name</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon ion-person"></i></span>
                                        <input type="text" name="lastname" class="form-control" id="lastname" placeholder="Last name">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="jobtitle" class="control-label sr-only">Job title</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon ion-android-contact"></i></span>
                                        <input type="text" name="jobtitle" class="form-control" id="jobtitle" placeholder="Job Title">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="control-label sr-only">Email address</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon ion-email"></i></span>
                                        <input type="text" name="email" class="form-control" id="email" placeholder="Email address">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="password" class="control-label sr-only">Password</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon ion-locked"></i></span>
                                        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password2" class="control-label sr-only">Confirm Password</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon ion-locked"></i></span>
                                        <input type="password" name="password2" class="form-control" id="password2" placeholder="Confirm Password">
                                        <?php echo csrf_field(); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-7">
                                    <button type="submit" class="btn btn-success btn-block">Create account</button>
                                </div>
                                <div class="col-xs-5 text-right">
                                    
                                </div>
                            </div>
                        
                        <button type="submit" class="btn btn-link btn-login-help"><i class="icon ion-help-circled"></i></button>
                        </form>
                    </div>
                    
                    @endif;
                </div>
                
            </div>
        </div>

        <!-- Javascript -->
        <script src="assets/js/jquery/jquery-2.1.0.min.js"></script>
        <script src="assets/js/bootstrap/bootstrap.js"></script>
        <script src="assets/js/queen-form-layouts.min.js"></script>

    </body>

    <!-- Mirrored from demo.thedevelovers.com/dashboard/queenadmin-1.1/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 12 Jun 2015 14:34:32 GMT -->
</html>