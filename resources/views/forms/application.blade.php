@extends('layout')
@section('content')
<div class="container-fluid primary-content">
    <!-- PRIMARY CONTENT HEADING -->
    <div class="primary-content-heading clearfix">
        <h2>{{ $title }}</h2>
        <ul class="breadcrumb pull-left">
            <li><i class="icon ion-home"></i><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Dashboard v1</li>
        </ul>
        <!-- quick task modal -->

        <!-- end quick task modal -->
    </div>
    <!-- END PRIMARY CONTENT HEADING -->

    <div class="widget widget-no-header widget-transparent bottom-30px">
        <!-- QUICK SUMMARY INFO -->

        <!-- END QUICK SUMMARY INFO -->
    </div>

    <div class="row">
        <div class="col-md-8">

            <div class="widget">
                <div class="widget-header clearfix">
                    <h3><i class="icon ion-android-arrow-down-right"></i> 
                        <span>Job details</span>
                    </h3>
                    <div class="btn-group widget-header-toolbar visible-lg">
                        <a href="#" title="Expand/Collapse" class="btn btn-link btn-toggle-expand"><i class="icon ion-ios7-arrow-up"></i></a>
                        <a href="#" title="Remove" class="btn btn-link btn-remove"><i class="icon ion-ios7-close-empty"></i></a>
                    </div>
                </div>
                <div class="widget-content">
                    <form action="{{url('/process_application')}}" method="post">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Names</label>
                                <div class="col-md-10">
                                    <input class="form-control" name="full_names" value="{{ $applicants_details->full_names }}" placeholder="Job title" type="text" disabled="true">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-2 control-label">Current stage</label>
                                <div class="col-md-10">
                                    
                                    <?php $status = 0; 
                                    
                                        if( $applicants_details->status == 0 ){
                                            $status = 'Pending application';
                                        }
                                        
                                        if( $applicants_details->status == 1 ){
                                            $status = 'Shortlisted';
                                        }
                                        
                                       if( $applicants_details->status == 2 ){
                                            $status = 'Interviewing';
                                       }
                                        
                                        if( $applicants_details->status == 3 ){
                                            $status = 'On-boarded';
                                        }
                                        ?>
                                   
                                    <input class="form-control" name="current stage" value="{{ $status }}"  type="text" disabled>
                                    
                                    <input type="hidden" name="user_id" value="{{$applicants_details->user_id}}" />
                                    <input type="hidden" name="id" value="{{$applicants_details->id}}" />
                                    <input type="hidden" name="job_id" value="{{$applicants_details->job_id}}" />
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <label class="col-md-2 control-label">Note</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" name="note" rows="4"></textarea>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-2 control-label">Move stage</label>
                                <div class="col-md-10">
                                    <select name="status"  class="form-control">
                                        <option value="0">Pending Application</option>
                                        <option value="1">Shortlist</option>
                                        <option value="2">Interview</option>
                                        <option value="3">On-board</option>
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <p class="form-control-static">
                                        <?php echo csrf_field(); ?>
                                        <input type="submit" name="" value="Save" class="btn btn-primary btn-sm">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection