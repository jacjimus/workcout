@extends('layout')
@section('content')
<div class="container-fluid primary-content">
    <!-- PRIMARY CONTENT HEADING -->
    <div class="primary-content-heading clearfix">
        <h2>{{ $title }}</h2>
        <ul class="breadcrumb pull-left">
            <li><i class="icon ion-home"></i><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Dashboard v1</li>
        </ul>
        <!-- quick task modal -->

        <!-- end quick task modal -->
    </div>
    <!-- END PRIMARY CONTENT HEADING -->

    <div class="widget widget-no-header widget-transparent bottom-30px">
        <!-- QUICK SUMMARY INFO -->

        <!-- END QUICK SUMMARY INFO -->
    </div>

    <div class="row">
        <div class="col-md-7">

            <div class="widget">
                <div class="widget-header clearfix">
                    <h3><i class="icon ion-android-arrow-down-right"></i> 
                        <span>System Users</span>
                    </h3>
                    <div class="btn-group widget-header-toolbar visible-lg">
                        <a href="#" title="Expand/Collapse" class="btn btn-link btn-toggle-expand"><i class="icon ion-ios7-arrow-up"></i></a>
                        <a href="#" title="Remove" class="btn btn-link btn-remove"><i class="icon ion-ios7-close-empty"></i></a>
                    </div>
                </div>
                <div class="widget-content">
                    <form action="{{url('/user')}}" method="post">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Email Address</label>
                                <div class="col-md-10">
                                    <input class="form-control" value='{{ isset($user_meta->email) ? $user_meta->email : ''}}' name="email" placeholder="Email address..." type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Full names</label>
                                <div class="col-md-10">
                                    <input class="form-control" name="full_names" value='{{ isset($user_meta->name) ? $user_meta->name : ''}}' placeholder="Full names..." type="text">
                                </div>
                            </div>
                            
                            <input type='hidden' name='user_id' value='{{ isset($user_meta->id) ? $user_meta->id : ''}}' />
                            @if($user_id == '')
                            <div class="form-group">
                                <label class="col-md-2 control-label">Password</label>
                                <div class="col-md-10">
                                    <input class="form-control" name="password" placeholder="Password..." type="password">
                                </div>
                            </div>
                            @endif
                            
                            <div class="form-group">
                                <label class="col-md-2 control-label">Status</label>
                                <div class="col-md-10">
                                    <select name="status" class="form-control">
                                        <option value="1" >Active</option>
                                        <option value="2" >Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Role</label>
                                <div class="col-md-10">
                                    <select name="role" data-placeholder="Full-Time" class="form-control">
                                        <option value="2">Back 0ffice</option>
                                        <option value="3">Super Admin</option>
                                    </select>
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <p class="form-control-static">
                                        <?php echo csrf_field(); ?>
                                        <input type="submit" name="" value="Save" class="btn btn-primary btn-sm">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection