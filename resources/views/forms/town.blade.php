@extends('layout')
@section('content')
<div class="container-fluid primary-content">
    <!-- PRIMARY CONTENT HEADING -->
    <div class="primary-content-heading clearfix">
        <h2>{{ $title }}</h2>
        <ul class="breadcrumb pull-left">
            <li><i class="icon ion-home"></i><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Dashboard v1</li>
        </ul>
        <!-- quick task modal -->

        <!-- end quick task modal -->
    </div>
    <!-- END PRIMARY CONTENT HEADING -->

    <div class="widget widget-no-header widget-transparent bottom-30px">
        <!-- QUICK SUMMARY INFO -->

        <!-- END QUICK SUMMARY INFO -->
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="widget">
                <div class="widget-header clearfix">
                    <h3><i class="icon ion-android-arrow-down-right"></i> 
                        <span>Town/City details</span>
                    </h3>
                    <div class="btn-group widget-header-toolbar visible-lg">
                        <a href="#" title="Expand/Collapse" class="btn btn-link btn-toggle-expand"><i class="icon ion-ios7-arrow-up"></i></a>
                        <a href="#" title="Remove" class="btn btn-link btn-remove"><i class="icon ion-ios7-close-empty"></i></a>
                    </div>
                </div>
                <div class="widget-content">
                    <form action="" method="post">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Town/ City</label>
                                <div class="col-md-10">
                                    <input class="form-control" name="title" placeholder="Town/City" type="text" value="{{$town->title}}">
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <p class="form-control-static">
                                        <?php echo csrf_field(); ?>
                                        <input type="submit" name="" value="Save" class="btn btn-primary btn-lg">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection