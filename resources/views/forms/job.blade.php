@extends('layout')
@section('content')
<div class="container-fluid primary-content">
    <!-- PRIMARY CONTENT HEADING -->
    <div class="primary-content-heading clearfix">
        <h2>{{ $title }}</h2>
        <ul class="breadcrumb pull-left">
            <li><i class="icon ion-home"></i><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Dashboard v1</li>
        </ul>
        <!-- quick task modal -->

        <!-- end quick task modal -->
    </div>
    <!-- END PRIMARY CONTENT HEADING -->

    <div class="widget widget-no-header widget-transparent bottom-30px">
        <!-- QUICK SUMMARY INFO -->

        <!-- END QUICK SUMMARY INFO -->
    </div>

    <div class="row">
        <div class="col-md-10">

            <div class="widget">
                <div class="widget-header clearfix">
                    <h3><i class="icon ion-android-arrow-down-right"></i> 
                        <span>Job details</span>
                    </h3>
                    <div class="btn-group widget-header-toolbar visible-lg">
                        <a href="#" title="Expand/Collapse" class="btn btn-link btn-toggle-expand"><i class="icon ion-ios7-arrow-up"></i></a>
                        <a href="#" title="Remove" class="btn btn-link btn-remove"><i class="icon ion-ios7-close-empty"></i></a>
                    </div>
                </div>
                <div class="widget-content">
                    <form action="{{url('/add_job')}}" method="post">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Job Title</label>
                                <div class="col-md-10">
                                    <input class="form-control" name="job_title" placeholder="Job title" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Company name</label>
                                <div class="col-md-10">
                                    <input class="form-control" name="company_name" placeholder="Company name" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Employer Summary</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" placeholder="textarea" rows="4"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Skills tag</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" name="skills" placeholder="skills separated by comma" rows="4"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Job Description</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" placeholder="textarea" name="job_description" rows="4"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Mini Job summary</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" placeholder="textarea" name="mini_summary" rows="4"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">City / Town</label>
                                <div class="col-md-10">
                                    <select name="town_id" data-placeholder="Full-Time" class="form-control">
                                        @foreach($towns as $town)
                                        <option value="{{$town->town_id}}" >{{$town->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Country</label>
                                <div class="col-md-10">
                                    <select name="country_id" data-placeholder="Full-Time" class="form-control">
                                        @foreach($countries as $country)
                                        <option value="{{$country->country_id}}">{{$country->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Industry</label>
                                <div class="col-md-10">
                                    <select name="industry_id" data-placeholder="Full-Time" class="form-control">
                                        @foreach($industries as $industry)
                                        <option value="{{$industry->industry_id}}">{{$industry->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            


                            <div class="form-group">
                                <label class="col-md-2 control-label">Salary range minimum</label>
                                <div class="col-md-10">
                                    <input class="form-control" name="salary_range_min" placeholder="Salary minimum" type="text">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-2 control-label">Salary range maximum</label>
                                <div class="col-md-10">
                                    <input class="form-control" name="salary_range_max" placeholder="Salary maximum" type="text">
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <label class="col-md-2 control-label">Currency</label>
                                <div class="col-md-10">
                                    <select name="currency_id" class="form-control">
                                        @foreach($currencies as $currency)
                                        <option value="{{$currency->currency_id}}">{{$currency->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-2 control-label">Job category</label>
                                <div class="col-md-10">
                                    <select name="job_category_id" data-placeholder="Full-Time" class="form-control">
                                        @foreach($categories as $category)
                                        <option value="{{$category->job_category_id}}">{{$category->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <p class="form-control-static">
                                        <?php echo csrf_field(); ?>
                                        <input type="submit" name="" value="Save" class="btn btn-primary btn-sm">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection